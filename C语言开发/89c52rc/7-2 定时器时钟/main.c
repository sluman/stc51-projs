#include <REGX52.H>
#include "Delay.h"
#include "LCD1602.h"
#include "Timer0.h"

unsigned char sec=55, min=59, hour=23;
// 检测数据：unsigned char sec = 55, min = 59, hour = 23;

void main()
{
    LCD_Init();
    Timer0Init();
    LCD_ShowString(1, 1, "Clock:");
    LCD_ShowString(2, 1, "  :  :");
	while (1)
	{
        // 不在中断函数内执行时间过长的任务，因此将LCD_ShowNum放到主函数中执行
        LCD_ShowNum(2, 1, hour, 2);
        LCD_ShowNum(2, 4, min, 2);
        LCD_ShowNum(2, 7, sec, 2);
	}
}

void Timer0_Rountine(void) interrupt 1
{
    static unsigned int t0_count;
    TL0 = 0x18;		//设置定时初始值
    TH0 = 0xFC;		//设置定时初始值
    t0_count++;
    if (t0_count >= 1000)
    {
        t0_count = 0;
        sec++;
        if (sec >= 60)
        {
            sec = 0;
            min++;
        }
        if (min >= 60)
        {
            min = 0;
            hour++;
        }
        if (hour >= 24)
        {
            hour = 0;
        }
    }
}