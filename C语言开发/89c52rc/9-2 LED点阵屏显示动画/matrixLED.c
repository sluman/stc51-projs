#include <REGX52.H>
#include "Delay.h"

sbit RCK = P3^5;  // RCLK
sbit SCK = P3^6;  // SRCLK
sbit SER = P3^4;

#define MATRIX_LED_PORT P0

/**
  * @brief  74HC595写入一个字节
  * @param  要写入的字节
  * @retval 无
  */
void _74HC595_writebyte(unsigned char byte)
{
    unsigned char i;
    for (i = 0; i < 8; i++)
    {
        SER = byte & (0x80 >> i);  // 第一次，最高位赋1，0x80 = 1000 0000
        /*
         * 对于将1位值赋给1位，SER = 1; or SER = 0; 都是直接赋值
         * 对于比如8位赋给1位，SER = 0x66; 则按逻辑赋值，非0即1，也可以用if条件语句进行判断
         */
        SCK = 1;  // 上升沿
        SCK = 0;  // 移入byte的第(i + 1)位后复位
    }
    RCK = 1;
    RCK = 0;
}

/**
  * @brief  点阵屏初始化
  * @param  无
  * @retval 无
  */
void matrixLED_init()
{
    SCK = 0;  // 上电SCK复位
    RCK = 0;
}

/**
  * @brief  LED点阵屏显示一列的数据
  * @param  column  要选择的列，范围：0 ~ 7，0在最左边
            Data    选择列显示的数据，高位在上，1为亮，0为灭
  * @retval 无
  */
void matrixLED_showclumn(unsigned char column, Data)
{
    _74HC595_writebyte(Data);  // 行共阳极 选中为1
    MATRIX_LED_PORT = ~(0x80 >> column);    // 列共阴极 位选 选中为0
    Delay(1);
    MATRIX_LED_PORT = 0xFF;  // 位清零
}
