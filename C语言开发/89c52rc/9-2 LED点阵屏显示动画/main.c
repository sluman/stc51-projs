#include <REGX52.H>
#include "Delay.h"
#include "matrixLED.h"

// 使用code关键字将数据放入flash中（无法更改）而非RAM中，节省内存
unsigned char code animation[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0xFF, 0x08, 0x08, 0x08, 0xFF, 0x00, 0x3F, 0x29, 0x29, 0x3B, 0x00, 0xFF, 
    0x00, 0xFF, 0x00, 0x3F, 0x21, 0x21, 0x3F, 0x00, 0x7D, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00};  // 前后各加8个0x00，使字幕移出后再移进

void main()
{
    unsigned char i, offset = 8, count = 0;
    matrixLED_init();
    while (1)
    {
        for (i = 0; i < 8; i++)
        {
            matrixLED_showclumn(i, animation[i + offset]);
        }
        // 使用计数方式延时，一帧扫描十遍，也可以用计时器的方法
        // 不能调用Delay()，因为扫描仍然需要快速进行
        count++;
        if (count > 10)
        {
            count = 0;
            offset++;
            if (offset > 32)  // 32 - 8 + 8，检验：i最大为24 + 7 = 31 < 32
            { 
                offset = 0;
            }
        }
    }
}