#include <REGX52.H>
#include "Delay.h"
#include "LCD1602.h"
#include "Timer0.h"
#include "IR.h"

unsigned char num, address, command;
//unsigned int time;

void main()
{
    LCD_Init();
    LCD_ShowString(1, 1, "ADDR  CMD  NUM");
    LCD_ShowString(2, 1, "00    00   000");
//    Int0_init();
    IR_init();
//    LCD_ShowNum(2, 1, time, 5);
	while (1)
	{
		if (IR_get_dataflag() || IR_get_repeatflag())
        {
            address = IR_get_address();
            command = IR_get_command();
            LCD_ShowHexNum(2, 1, address, 2);
            LCD_ShowHexNum(2, 7, command, 2);
            if (command == IR_VOL_LOW)
            {
                num--;
            }
            if (command == IR_VOL_HIGH)
            {
                num++;
            }
            LCD_ShowNum(2, 12, num, 3);
        }
	}
}

//void Int0_Routine(void) interrupt 0
//{
//    num--;
//}