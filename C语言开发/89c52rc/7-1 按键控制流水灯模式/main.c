#include <REGX52.h>
#include "Timer0.h"  // STC-ISP对新系列单片机配置
#include "Key.h"
#include <INTRINS.H>

unsigned char key_number, led_mode;

/*
自己配置
void Timer0_Init() {
    // 配置TMOD 不可位寻址
    // 直接赋值法 不好，操作四位会改变另外四位
    //TMOD = 0x01;  // 0000 0001
    // 与或式赋值法
    TMOD &= 0xF0;  // ____ ____ & 1111 0000B：把TMOD的低四位清零，高四位保持不变
    TMOD |= 0x01;  // ____ ____ | 0000 0001B：把TMOD最低位置1，高四位保持不变
    // 配置TCON 可位寻址
    TF0 = 0;       // 清零。防止一开始产生中断
    TR0 = 1;       // 定时器开始工作
    // 配置TL0和TH0
    // 计数器0 ~ 65535，赋初值64535D = 1111 1100 0001 0111B = FC17H，离计数器溢出差值为1000μs，故计时时间为1ms
    // 和STC-ISP的配置有1μs的差别
    TH0 = 64535 / 256;  // 除以2^8，拿出高8位 0xFC
    TL0 = 64535 % 256;  // 对2^8取余，拿出低8位 0x17，实际上应该+1，65535+1 = 0溢出那一刻也算1μs，1001μs
    // 配置IE，允许中断
    ET0 = 1;
    EA = 1;
    // 配置IP，自然优先级低级
    PT0 = 0;
}
*/

void main() {
    P2 = 0xFE;  //点亮最低位LED
    Timer0Init();
    while (1) {
        key_number = Key();
        if (key_number) {
            /* 测试按键Key函数是否正确
                !符号是“位”取反，只针对位变量；~符号是“按位”取反，针对字节变量，否则会有错误：using ~ on bit/bool/unsigned char variables
                if (key_number == 1) P2_1 = !P2_1;
                if (key_number == 2) P2_2 = !P2_2;
                if (key_number == 3) P2_3 = !P2_3;
                if (key_number == 4) P2_4 = !P2_4;
            */
            // 控制led_mode只会按1, 0, 1, 0, … 变化
            if (key_number == 1) {
                led_mode++;
                if (led_mode >= 2) led_mode = 0;
            }
        }
    }
}

void Timer0_Rountine(void) interrupt 1 {
    // 将全局变量放到里面冠以static关键字，与全局变量可以在整个工程中应用不同，static变量只能在本源文件中应用
    static unsigned int t0_count;
    // “沙漏”计时一次完成后，值会溢出，人为“倒转”赋初值后再重新开始
    /*
    TH0 = 64535 / 256;
    TL0 = 64535 % 256;
    */
    TL0 = 0x18;		//设置定时初始值
    TH0 = 0xFC;		//设置定时初始值
    t0_count++;
    if (t0_count >= 500) {
        t0_count = 0;
		// 使用_crol_(unsigned char, unsigned char)和_cror_(unsigned char, unsigned char)进行不进位循环左右位移运算，与>>, <<不同
        if (led_mode == 0) P2 = _crol_(P2, 1);
        if (led_mode == 1) P2 = _cror_(P2, 1);
    }
}