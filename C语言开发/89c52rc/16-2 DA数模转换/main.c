#include <REGX52.H>
#include "Delay.h"
#include "Timer0.h"

sbit DA = P2^1;

unsigned char counter, compare, i;

void main()
{
    Timer0_Init();
    compare = 5;
	while (1)
	{
		for (i = 0; i < 100; i++)
        {
            compare = i;
            Delay(10);
        }
        for (i = 100; i > 0; i--)
        {
            compare = i;
            Delay(10);
        }
	}
}

// 每隔100μs进入1次中断，为主循环匀出空间
void Timer0_Rountine(void) interrupt 1
{
    TL0 = 0x9C;		//设置定时初始值
    TH0 = 0xFF;		//设置定时初始值
    counter++;
    counter %= 100;
    if (counter < compare)
    {
        DA = 1;
    }
    else
    {
        DA = 0;
    }
}
