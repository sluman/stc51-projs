#include <REGX52.H>

sbit LCD_RS = P2^6;
sbit LCD_RW = P2^5;
sbit LCD_E = P2^7;
#define LCD_DATA_PORT P0

void LCD_delay()		//@12.000MHz 1ms
{
	unsigned char i, j;

	i = 2;
	j = 239;
	do
	{
		while (--j) {}
	} while (--i);
}

void LCD_writecommand(unsigned char command)
{
    LCD_RS = 0;
    LCD_RW = 0;
    LCD_DATA_PORT = command;
    LCD_E = 1;
    LCD_delay();
    LCD_E = 0;
    LCD_delay();
}

void LCD_writedata(unsigned char Data)
{
    LCD_RS = 1;
    LCD_RW = 0;
    LCD_DATA_PORT = Data;
    LCD_E = 1;
    LCD_delay();
    LCD_E = 0;
    LCD_delay();
}

void LCD_init(void)
{
    // 8位数据接口，两行显示，5 × 7点阵
    LCD_writecommand(0x38);
    // 显示开，光标关，闪烁关
    LCD_writecommand(0x0C);
    // 数据读写操作后，光标自动加一，画面不动
    LCD_writecommand(0x06);
    // 清屏
    LCD_writecommand(0x01);
}

void LCD_setcursor(unsigned char line, unsigned char column)
{
    if (line == 1)
    {
        // line为1时column - 1为实际地址
        LCD_writecommand(0x80 | (column - 1));
    }
    else
    {
        LCD_writecommand(0x80 | (column - 1) + 0x40);
    }
}

void LCD_showchar(unsigned char line, unsigned char column, unsigned char Char)
{
    LCD_setcursor(line, column);
    LCD_writedata(Char);
}

void LCD_showstring(unsigned char line, unsigned char column, unsigned char *string)
{
    unsigned char i;
    LCD_setcursor(line, column);
    for (i = 0; string[i] != '\0'; i++)
    {
        LCD_writedata(string[i]);
    }
}

int LCD_pow(int x, int y)
{
    unsigned char i;
    int result = 1;
    for (i = 0; i < y; i++)
    {
        result *= x;
    }
    return result;
}

void LCD_shownum(unsigned char line, unsigned char column, unsigned int number, unsigned char length)
{
    unsigned char i;
    LCD_setcursor(line, column);
    for (i = length; i > 0; i--)
    {
        LCD_writedata(0x30 + number / LCD_pow(10, i - 1) % 10);  // 记得将数字转换成ASCII码 '0' = 0x30
    }
}

void LCD_show_signednum(unsigned char line, unsigned char column, int number, unsigned char length)
{
    unsigned char i;
    unsigned int number1;
    LCD_setcursor(line, column);
    if (number >= 0)
    {
        LCD_writedata('+');
        number1 = number;
    }
    else
    {
        LCD_writedata('-');
        number1 = -number;  // 防止-32768不能用int(-2^15 ~ 32767)存储
    }
    for (i = length; i > 0; i--)
    {
        LCD_writedata(0x30 + number1 / LCD_pow(10, i - 1) % 10);
    }
}

void LCD_show_hexnum(unsigned char line, unsigned char column, unsigned int number, unsigned char length)
{
    unsigned char i;
    unsigned char singleNumber;
    LCD_setcursor(line, column);
    for (i = length; i > 0; i--)
    {
        singleNumber = number / LCD_pow(16, i - 1) % 16;
        if (singleNumber < 10)
        {
            LCD_writedata('0' + singleNumber);
        }
        else
        {
            LCD_writedata('A' + singleNumber - 10);
        }
    }
}

void LCD_show_binnum(unsigned char line, unsigned char column, unsigned int number, unsigned char length)
{
    unsigned char i;
    LCD_setcursor(line, column);
    for (i = length; i > 0; i--)
    {
        LCD_writedata('0' + number / LCD_pow(2, i - 1) % 2);
    }
}