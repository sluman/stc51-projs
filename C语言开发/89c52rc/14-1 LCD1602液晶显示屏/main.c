#include <REGX52.H>
#include "LCD1602.h"
#include "Delay.h"

void main()
{
    LCD_init();
    LCD_showchar(1, 1, 0x41);  // 'A'
    LCD_showstring(1, 3, "Hello");
    LCD_shownum(1, 9, 66, 2);
    LCD_show_signednum(1, 12, -88, 2);
    LCD_show_hexnum(2, 1, 0xA5, 2);
    LCD_show_binnum(2, 4, 0xA5, 8);
    LCD_showchar(2, 13, 0xDF);  // 11011111
    LCD_showchar(2, 14, 'C');  // 11100000
    LCD_showchar(2, 16, 0xE0);
    LCD_showstring(1, 16, "Welcome to New York!");
    
	while (1)
	{
		LCD_writecommand(0x18);
        Delay(500);
	}
}