#ifndef __LCD1602_H__
#define __LCD1602_H__

void LCD_delay();
void LCD_writecommand(unsigned char command);
void LCD_writedata(unsigned char Data);
void LCD_init(void);
void LCD_showchar(unsigned char line, unsigned char column, unsigned char Char);
void LCD_showstring(unsigned char line, unsigned char column, unsigned char *string);
void LCD_shownum(unsigned char line, unsigned char column, unsigned int number, unsigned char length);
void LCD_show_signednum(unsigned char line, unsigned char column, int number, unsigned char length);
void LCD_show_hexnum(unsigned char line, unsigned char column, unsigned int number, unsigned char length);
void LCD_show_binnum(unsigned char line, unsigned char column, unsigned int number, unsigned char length);

#endif