#ifndef __XPT2046_H__
#define __XPT2046_H__

#define XPT2046_XP_8    0x9C  // 10011100; 0x8C = 10001100
#define XPT2046_YP_8    0xDC  // 11011100
#define XPT2046_VBAT_8  0xAC  // 10101100
#define XPT2046_AUX_8   0xEC  // 11101100
#define XPT2046_XP_12   0x94  // 10010100
#define XPT2046_YP_12   0xD4  // 11010100
#define XPT2046_VBAT_12 0xA4  // 10100100
#define XPT2046_AUX_12  0xE4  // 11100100

unsigned int XPT2046_readAD(unsigned char command);

#endif