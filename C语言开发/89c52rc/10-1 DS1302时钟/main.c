#include <REGX52.H>
#include "LCD1602.h"
#include "DS1302.h"
#include "Delay.h"

unsigned char second, minute;

void main()
{
    LCD_Init();
    DS1302_init();
    //DS1302_writebyte(0x8E, 0x00);  // 关闭写保护位
    //LCD_ShowString(1, 1, "RTC");
    LCD_ShowString(1, 1, "  -  -  ");
    LCD_ShowString(2, 1, "  :  :  ");
    DS1302_settime();
    
	while (1)
    {
        DS1302_readtime();
        //second = DS1302_readbyte(0x81);
        //minute = DS1302_readbyte(0x83);
        LCD_ShowNum(1, 1, DS1302_time[0], 2);
        //Delay(50);
        LCD_ShowNum(1, 4, DS1302_time[1], 2);
        LCD_ShowNum(1, 7, DS1302_time[2], 2);
        LCD_ShowNum(2, 1, DS1302_time[3], 2);
        LCD_ShowNum(2, 4, DS1302_time[4], 2);
        LCD_ShowNum(2, 7, DS1302_time[5], 2);
    }
}