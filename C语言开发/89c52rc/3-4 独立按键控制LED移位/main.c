#include <REGX52.H>

void Delay(unsigned int);
unsigned char LEDNum;

void main() {
    // 上电初始化
    // 不能写到死循环里面，不然所有的if失效，因为总有D1灯亮可以执行，结果按键无效且D1长亮
    P2 = ~0x01;
    while (1) {
        // K1 P3_1 板上接线顺序错误，将错就错
        if (P3_1 == 0) {
            Delay(20);
            while (P3_1 == 0) {}
            Delay(20);
            // 由于全局变量LEDNum初始默认是0，因此先判断溢出
            if (LEDNum == 0) {
                LEDNum = 7;
            } else {
                LEDNum--;
            }
            // 由于0为亮，1为灭，与二进制递增移位正好相反，因此采用按位取反
            P2 = ~(0x01 << LEDNum);
        }
        // K2 P3_0
        if (P3_0 == 0) {
            Delay(20);
            while (P3_0 == 0) {}
            Delay(20);
            // 向右移动
            LEDNum++;
            if (LEDNum >= 8) {
                LEDNum = 0;
            }
            // 由于0为亮，1为灭，与二进制递增移位正好相反，因此采用按位取反
            P2 = ~(0x01 << LEDNum);
        }
    }
}

void Delay(unsigned int xms) {
    unsigned char i, j;
    while (xms) {
        i = 2;
        j = 239;
        do {
            while (--j);
        } while (--i);
        xms--;
    }
}