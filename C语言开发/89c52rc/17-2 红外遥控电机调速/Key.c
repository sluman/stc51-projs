#include <REGX52.h>
#include "Delay.h"

unsigned char Key_keyNumber;

unsigned char Key(void)
{
    unsigned char temp = 0;
    temp = Key_keyNumber;
    Key_keyNumber = 0;
    return temp;
}

unsigned char Key_getstate()
{
    unsigned char keyNumber = 0;
    if (P3_1 == 0)
    {
        keyNumber = 1;
    }
    if (P3_0 == 0)
    {
        keyNumber = 2;
    }
    if (P3_2 == 0)
    {
        keyNumber = 3;
    }
    if (P3_3 == 0)
    {
        keyNumber = 4;
    }
    return keyNumber;
}

/**
  * @brief  Key自己的循环调用的中断函数
  * @param  无
  * @retval 无
  */
void Key_loop(void)
{
    static unsigned char nowState, lastState;
    lastState = nowState;
    nowState = Key_getstate();
    if (lastState == 1 && nowState == 0)
    {
        Key_keyNumber = 1;
    }
    if (lastState == 2 && nowState == 0)
    {
        Key_keyNumber = 2;
    }
    if (lastState == 3 && nowState == 0)
    {
        Key_keyNumber = 3;
    }
    if (lastState == 4 && nowState == 0)
    {
        Key_keyNumber = 4;
    }
}