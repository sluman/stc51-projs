#include <REGX52.h>

/*
 * @brief  定时器1初始化，100μs@12.000MHz
 * @param  无
 * @retval 无
 */
 // 电机频率在10k ~ 20kHz(单片机达不到，1k附近会产生蜂鸣)，若计数器为100，则T = 100 * 100 μs = 10 ms
 // 将定时器0的高四位与低四位互换即为定时器1
void Timer1_Init(void)
{
    TMOD &= 0x0F;		//设置定时器模式
    TMOD |= 0x10;		//设置定时器模式
    TL1 = 0x9C;		//设置定时初始值
    TH1 = 0xFF;		//设置定时初始值
    TF1 = 0;		//清除TF0标志
    TR1 = 1;		//定时器0开始计时
    ET1 = 1;
    EA = 1;
    PT1 = 0;
}

/* 定时器中断函数模板
void Timer1_Rountine(void) interrupt 3
{
    static unsigned int T1_count;
    TL1 = 0x9C;		//设置定时初始值
    TH1 = 0xFF;		//设置定时初始值
    T1_count++;
    if (T1_count >= 1000)
    {
        T1_count = 0;
    }
}
*/