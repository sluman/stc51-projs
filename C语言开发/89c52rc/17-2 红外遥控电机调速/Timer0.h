#ifndef __TIMER0_H__
#define __TIMER0_H__

void Timer0_Init(void);
void Timer0_set_counter(unsigned int value);
unsigned int Timer0_get_counter(void);
void Timer0_run(unsigned char flag);

#endif