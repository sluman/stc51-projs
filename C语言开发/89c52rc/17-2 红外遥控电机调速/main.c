#include <REGX52.H>
#include "Delay.h"
#include "Key.h"
#include "nixieTube.h"
#include "motor.h"
#include "IR.h"

unsigned char command, speed;

void main()
{
    motor_init();
    IR_init();
	while (1)
	{
        if (IR_get_dataflag())
        {
            command = IR_get_command();
            if (command == IR_0)
            {
                speed = 0;
            }
            if (command == IR_1)
            {
                speed = 1;
            }
            if (command == IR_2)
            {
                speed = 2;
            }
            if (command == IR_3)
            {
                speed = 3;
            }
            
            if (speed == 0)
            {
                motor_setspeed(0);
            }
            if (speed == 1)
            {
                motor_setspeed(50);
            }
            if (speed == 2)
            {
                motor_setspeed(75);
            }
            if (speed == 3)
            {
                motor_setspeed(100);
            }
        }
        nixieTube(1, speed);
	}
}
