#include <REGX52.H>
#include "Timer1.h"

sbit MOTOR = P1^0;

unsigned char counter, compare;

void motor_init()
{
    Timer1_init();
}

void motor_setspeed(unsigned char speed)
{
    compare = speed;
}

// 每隔100μs进入1次中断，为主循环匀出空间
void Timer1_Rountine(void) interrupt 3
{
    TL1 = 0x9C;		//设置定时初始值
    TH1 = 0xFF;		//设置定时初始值
    counter++;
    //counter %= 100;
    if (counter >= 100)
    {    
        counter = 0;
        Key_loop();
    }
    if (counter < compare)
    {
        MOTOR = 1;  // 注意和LED极性相反，给1电机转
    }
    else
    {
        MOTOR = 0;
    }
}