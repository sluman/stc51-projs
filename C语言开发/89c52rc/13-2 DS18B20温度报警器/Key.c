#include <REGX52.h>
#include "Delay.h"

unsigned char Key_keyNumber;

/**
  * @brief  获取按键键码
  * @param  无
  * @retval 按下按键的键码，范围：0, 1 ~ 4, 0表示无按键按下
  */
unsigned char Key(void)
{
    unsigned char temp = 0;
    temp = Key_keyNumber;
    Key_keyNumber = 0;
    return temp;
}

/**
  * @brief  获取当前按键的状态，无消抖及松手检测
  * @param  无
  * @retval 按下按键的键码，范围：0, 1 ~ 4, 0表示无按键按下
  */
unsigned char Key_getstate()
{
    unsigned char keyNumber = 0;
    if (P3_1 == 0)
    {
        keyNumber = 1;
    }
    if (P3_0 == 0)
    {
        keyNumber = 2;
    }
    if (P3_2 == 0)
    {
        keyNumber = 3;
    }
    if (P3_3 == 0)
    {
        keyNumber = 4;
    }
    return keyNumber;
}

/**
  * @brief  按键驱动函数，Key自己的循环调用的中断函数
  * @param  无
  * @retval 无
  */
void Key_loop(void)
{
    static unsigned char nowState, lastState;
    lastState = nowState;       // 按键状态更新
    nowState = Key_getstate();  // 获取当前按键状态
    // 如果上个时间点按键按下，这个时间点未按下，则是松手瞬间，以此避免消抖和松手检测
    if (lastState == 1 && nowState == 0)
    {
        Key_keyNumber = 1;
    }
    if (lastState == 2 && nowState == 0)
    {
        Key_keyNumber = 2;
    }
    if (lastState == 3 && nowState == 0)
    {
        Key_keyNumber = 3;
    }
    if (lastState == 4 && nowState == 0)
    {
        Key_keyNumber = 4;
    }
}