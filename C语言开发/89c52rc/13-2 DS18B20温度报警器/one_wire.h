#ifndef __ONE_WIRE_H__
#define __ONE_WIRE_H__

bit one_wire_init();
void one_wire_sendbit(unsigned char Bit);
unsigned char one_wire_receivebit(void);
void one_wire_sendbyte(unsigned char byte);
unsigned char one_wire_receivebyte(void);

#endif