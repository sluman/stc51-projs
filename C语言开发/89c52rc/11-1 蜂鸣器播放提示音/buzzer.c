#include <REGX52.H>
#include <INTRINS.H>
#include "Delay.h"

// 蜂鸣器端口:
sbit BUZZER = P2^5;

/**
  * @brief  蜂鸣器私有延时函数，延时500us
  * @param  无
  * @retval 无
  */
void buzzer_delay500us()		//@11.0592MHz
{
	unsigned char i;

	_nop_();
	i = 227;
	while (--i);
}

/**
  * @brief  蜂鸣器鸣响
  * @param  ms 发生的时长
  * @retval 无
  */
void buzzer_time(unsigned int xms)
{
    unsigned int i;
	for (i = 0; i < xms * 2; i++)  // 之前循环对于1ms，现在变成0.5ms，需要循环两次 * 2
    {
        BUZZER = !BUZZER;
        buzzer_delay500us();
    }
}