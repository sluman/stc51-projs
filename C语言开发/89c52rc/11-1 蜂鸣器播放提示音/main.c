#include <REGX52.H>
#include "Delay.h"
#include "Key.h"
#include "nixieTube.h"
#include "buzzer.h"

unsigned char key_num;

void main()
{
    nixieTube(1, 0);
	while (1)
	{
		key_num = Key();
        if (key_num) 
        {
            buzzer_time(100);
            nixieTube(1, key_num);
        }
	}
}