#include <REGX52.H>

//@12.000MHz
void Delay(unsigned int xms) {
	unsigned char i, j;

	while (xms) {
		i = 2;
		j = 239;
		do {
			while (--j);
		} while (--i);
		xms--;
	}
}

void main() {
	while (1) {
		// 按下
		if (P3_1 == 0) {
			Delay(20);
			// 检测松手，一直按下则一直循环，一旦松手while不成立则跳出循环
			while (P3_1 == 0) {};
			Delay(20);
			// 通过按位取反，控制亮暗交替，否则按下变后只保持一种状态
			P2_0 = ~P2_0;
		}
	}
}