#include <REGX52.H>

void Delay(unsigned int xms) {
    unsigned char i, j;

    while (xms) {
        i = 2;
        j = 239;
        do {
            while (--j);
        } while (--i);
        xms--;
    }
}

void main() {
    // 用8位的unsigned char来表示1个寄存器
    unsigned char LEDNum = 0;
    while (1) {
        if (P3_1 == 0) {
            Delay(20);
            while (P3_1 == 0) {};
            Delay(20);
            // 8位总线操作
            /*
            // 1111 1111
            P2++;
            // 已经溢出0000 0000
            P2 = ~P2;
            // 1111 1111
            */
            LEDNum++;
            // 读出LEDNum对P2进行写操作，不对本身变量改变
            P2 = ~LEDNum;
        }
    }
}