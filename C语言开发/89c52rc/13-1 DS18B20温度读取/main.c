#include <REGX52.H>
#include "LCD1602.h"
//#include "one_wire.h"
#include "DS18B20.h"
#include "Delay.h"

unsigned char ack;
float t;

void main()
{
    DS18B20_convert_t();
    Delay(1000);
    LCD_Init();
    LCD_ShowString(1, 1, "Temperature:");
    ack = one_wire_init();
    LCD_ShowNum(2, 1, ack, 3);
	while (1)
	{
		DS18B20_convert_t();
        t = DS18B20_read_t();
        if (t < 0)
        {
            LCD_ShowChar(2, 1, '-');
            t = -t;
        }
        else
        {
            LCD_ShowChar(2, 1, '+');
        }
        LCD_ShowNum(2, 2, t, 3);
        LCD_ShowChar(2, 5, '.');
        LCD_ShowNum(2, 6, (unsigned long)(t * 10000) % 10000, 4);
	}
}