#include <REGX52.H>
#include "one_wire.h"
#include "LCD1602.h"

#define DS18B20_SKIP_ROM        0xCC
#define DS18B20_CONVERT_T       0x44
#define DS18B20_READ_SCRATCHPAD 0xBE

void DS18B20_convert_t(void)
{
    one_wire_init();
    one_wire_sendbyte(DS18B20_SKIP_ROM);
    one_wire_sendbyte(DS18B20_CONVERT_T);
}

float DS18B20_read_t(void)
{
    unsigned char TLSB, TMSB;
    int temp;
    float t;
    one_wire_init();
    one_wire_sendbyte(DS18B20_SKIP_ROM);
    one_wire_sendbyte(DS18B20_READ_SCRATCHPAD);
    TLSB = one_wire_receivebyte();  // 低8位
    TMSB = one_wire_receivebyte();  // 高8位
    LCD_ShowBinNum(1, 1, TMSB, 8);
    LCD_ShowBinNum(1, 9, TLSB, 8);
    temp = (TMSB << 8) | TLSB;
    t = temp / 16.0;  // 后四位为小数，故前移至int 整形提升，考虑精度损失，用float
    return t;
}