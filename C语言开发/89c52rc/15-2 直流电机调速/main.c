#include <REGX52.H>
#include "Delay.h"
#include "Key.h"
#include "nixieTube.h"
#include "Timer0.h"

sbit MOTOR = P1^0;

unsigned char counter, compare, keyNum, speed;

void main()
{
    Timer0_Init();
    compare = 5;
	while (1)
	{
		keyNum = Key();
        if (keyNum == 1)
        {
            speed++;
            speed %= 4;
            if (speed == 0)
            {
                compare = 0;
            }
            if (speed == 1)
            {
                compare = 50;
            }
            if (speed == 2)
            {
                compare = 75;
            }
            if (speed == 3)
            {
                compare = 100;
            }
        }
        nixieTube(1, speed);
	}
}

// 每隔100μs进入1次中断，为主循环匀出空间
void Timer0_Rountine(void) interrupt 1
{
    TL0 = 0x9C;		//设置定时初始值
    TH0 = 0xFF;		//设置定时初始值
    counter++;
    //counter %= 100;
    if (counter >= 100)
    {    
        counter = 0;
        Key_loop();
    }
    if (counter < compare)
    {
        MOTOR = 1;  // 注意和LED极性相反，给1电机转
    }
    else
    {
        MOTOR = 0;
    }
}
