#include <REGX52.h>

/*
 * @brief  定时器0初始化，100μs@12.000MHz
 * @param  无
 * @retval 无
 */
 // 电机频率在10k ~ 20kHz(单片机达不到，1k附近会产生蜂鸣)，若计数器为100，则T = 100 * 100 μs = 10 ms
void Timer0_Init(void)
{
    TMOD &= 0xF0;		//设置定时器模式
    TMOD |= 0x01;		//设置定时器模式
    TL0 = 0x9C;		//设置定时初始值
    TH0 = 0xFF;		//设置定时初始值
    TF0 = 0;		//清除TF0标志
    TR0 = 1;		//定时器0开始计时
    ET0 = 1;
    EA = 1;
    PT0 = 0;
}

/* 定时器中断函数模板
void Timer0_Rountine(void) interrupt 1
{
    static unsigned int t0_count;
    TL0 = 0x9C;		//设置定时初始值
    TH0 = 0xFF;		//设置定时初始值
    t0_count++;
    if (t0_count >= 1000)
    {
        t0_count = 0;
    }
}
*/