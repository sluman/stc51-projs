// 使用sdcc自带的通用C52头文件
#include <mcs51/8052.h>

unsigned char nixieTable[] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F};

void Delay(unsigned char xms) {
	while (xms--) {
		unsigned char i, j;
		i = 2;
		j = 239;
		do {
			while (--j) {}
		} while (--i);
	}

}

void nixieTube(unsigned char location, unsigned char number) {
	switch (location) {
		case 1:
			P2_4 = 1; P2_3 = 1; P2_2 = 1;
			break;
		case 2:
			P2_4 = 1; P2_3 = 1; P2_2 = 0;
			break;
		case 3:
			P2_4 = 1; P2_3 = 0; P2_2 = 1;
			break;
		case 4:
			P2_4 = 1; P2_3 = 0; P2_2 = 0;
			break;
		case 5:
			P2_4 = 0; P2_3 = 1; P2_2 = 1;
			break;
		case 6:
			P2_4 = 0; P2_3 = 1; P2_2 = 0;
			break;
		case 7:
			P2_4 = 0; P2_3 = 0; P2_2 = 1;
			break;
		case 8:
			P2_4 = 0; P2_3 = 0; P2_2 = 0;
		default:
			break;
	}
	P0 = nixieTable[number];
	/*
	 * 消影
	 * 位选 段选 位选 段选 位选 段选
	 * 在段选到位选出现问题，上一位会串到下一位
	 * 解决：上一位段选后先清零
	 */
	Delay(1);
	P0 = 0x00; // 空 清零
}

void main() {
	while (1) {
		nixieTube(1, 1);
//		Delay(20);
		nixieTube(2, 2);
//		Delay(20);
		nixieTube(3, 3);
//		Delay(20);
	}
}
