#include <REGX52.H>
#include "I2C.h"  // 类的继承思想

#define AT24C02_ADDRESS 0xA0

/**
  * @brief  AT24C02写入一个字节
  * @param  wordAddress 要写入字节的地址
            Data        要写入的数据
  * @retval 无
  */
void AT24C02_writebyte(unsigned char wordAddress, Data)
{
//    unsigned char ack;
    I2C_start();
    I2C_sendbyte(AT24C02_ADDRESS);
//    ack = I2C_receiveack();
//    if (ack == 0)
//    {
//        P2 = 0x00;
//    }
    I2C_receiveack();
    I2C_sendbyte(wordAddress);
    I2C_receiveack();
    I2C_sendbyte(Data);
    I2C_receiveack();
    I2C_stop();
}

/**
  * @brief  AT24C02读取一个字节
  * @param  wordAddress 要读出字节的地址
  * @retval 读出的数据
  */
unsigned char AT24C02_readbyte(unsigned char wordAddress)
{
    unsigned char Data;
    I2C_start();
    I2C_sendbyte(AT24C02_ADDRESS);
    I2C_receiveack();
    I2C_sendbyte(wordAddress);
    I2C_receiveack();
    I2C_start();
    I2C_sendbyte(AT24C02_ADDRESS | 0x01);  // 写地址
    I2C_receiveack();
    Data = I2C_receivebyte();
    I2C_sendack(1);  // 发送应答位1
    I2C_stop();
    return Data;
}