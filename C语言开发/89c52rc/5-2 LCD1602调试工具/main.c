#include <REGX52.H>
#include "LCD1602.h"
#include "Delay.h"

int result;

void main() {
    LCD_Init();
    LCD_ShowChar(1, 1, 'A');
    LCD_ShowString(1, 3, "MCU");
    LCD_ShowNum(1, 7, 1, 3);
    LCD_ShowSignedNum(1, 11, -66, 2);
    LCD_ShowHexNum(2, 1, 0xA8, 2);
    LCD_ShowBinNum(2, 4, 0xAA, 8);  // 10101010B

    result = 1 + 1;
    while (1) {
        result++;
        Delay(1000);
        LCD_ShowNum(2, 13, result, 3);
    }
}