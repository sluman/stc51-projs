#include <REGX52.H>
#include "Delay.h"

unsigned char nixie_buf[9] = {0, 10, 10, 10, 10, 10, 10, 10, 10};
unsigned char nixieTable[] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, 0x00, 0x40};

void nixie_setbuf(unsigned char location, unsigned char number)
{
    nixie_buf[location] = number;
}

void nixie_scan(unsigned char location, unsigned char number)
{
    // 段清零
    P0 = 0x00;
    // 位选
    switch (location)
    {
    case 1:
        P2_4 = 1;
        P2_3 = 1;
        P2_2 = 1;
        break;
    case 2:
        P2_4 = 1;
        P2_3 = 1;
        P2_2 = 0;
        break;
    case 3:
        P2_4 = 1;
        P2_3 = 0;
        P2_2 = 1;
        break;
    case 4:
        P2_4 = 1;
        P2_3 = 0;
        P2_2 = 0;
        break;
    case 5:
        P2_4 = 0;
        P2_3 = 1;
        P2_2 = 1;
        break;
    case 6:
        P2_4 = 0;
        P2_3 = 1;
        P2_2 = 0;
        break;
    case 7:
        P2_4 = 0;
        P2_3 = 0;
        P2_2 = 1;
        break;
    // 第8位是LED1，对应Y0
    case 8:
        P2_4 = 0;
        P2_3 = 0;
        P2_2 = 0;
        break;
    default:
        break;
    }
    // 段选
    P0 = nixieTable[number];
}

/**
  * @brief  每隔x ms调用起到原先在nixieTube中Delay(x)的作用
  * @param  无
  * @retval 无
  */
void nixie_loop(void)
{
    static unsigned char i = 1;
    nixie_scan(i, nixie_buf[i]);
    i++;
    if (i >= 9)
    {
        i = 1;
    }
}