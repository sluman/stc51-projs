#ifndef __NIXIETUBE_H__
#define __NIXIETUBE_H__

void nixie_scan(unsigned char, unsigned char);
void Nixie_loop(void);
void nixie_setbuf(unsigned char location, unsigned char number);

#endif