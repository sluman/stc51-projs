#include <REGX52.H>

unsigned char nixieTable[] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F};
//unsigned char nixieCharTable[] = {0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71, 0x00};

// 封装nixieTube数码管函数
void nixieTube(unsigned char location, unsigned char number) {
    switch (location) {
        case 1:
            P2_4 = 1; P2_3 = 1; P2_2 = 1;
            break;
        case 2:
            P2_4 = 1; P2_3 = 1; P2_2 = 0;
            break;
        case 3:
            P2_4 = 1; P2_3 = 0; P2_2 = 1;
            break;
        case 4:
            P2_4 = 1; P2_3 = 0; P2_2 = 0;
            break;
        case 5:
            P2_4 = 0; P2_3 = 1; P2_2 = 1;
            break;
        case 6:
            P2_4 = 0; P2_3 = 1; P2_2 = 0;
            break;
        case 7:
            P2_4 = 0; P2_3 = 0; P2_2 = 1;
            break;
        // 第8位是LED1，对应Y0
        case 8:
            P2_4 = 0; P2_3 = 0; P2_2 = 0;
            break;
        default:
            break;
    }
    P0 = nixieTable[number];
    //P0 = nixieCharTable[number];
}

void main() {
    /*
    // Y5对应101，选中LED6
    P2_4 = 1;
    P2_3 = 0;
    P2_2 = 1;
    // LED5显示6 0111 1101 = 7D
    P0 = 0x7D;
    */
    nixieTube(8, 0);
    while (1) {}
}