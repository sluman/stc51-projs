#include <8052.h>

/*
 * @brief  定时器0初始化，1毫秒@12.000MHz
 * @param  无
 * @retval 无
 */

void Timer0Init(void)		//1毫秒@12MHz
{
    //AUXR &= 0x7F;		//定时器时钟12T模式，新系列有AUXR寄存器，本次不设置
    TMOD &= 0xF0;		//设置定时器模式
    TMOD |= 0x01;		//设置定时器模式
    TL0 = 0x18;		//设置定时初始值
    TH0 = 0xFC;		//设置定时初始值
    TF0 = 0;		//清除TF0标志
    TR0 = 1;		//定时器0开始计时
    // 加上自己对中断的配置
    ET0 = 1;
    EA = 1;
    PT0 = 0;  // 可写可不写，默认为0
}

/* 定时器中断函数模板
void Timer0_Rountine(void) interrupt 1 
{
    static unsigned int t0_count;
    TL0 = 0x18;		//设置定时初始值
    TH0 = 0xFC;		//设置定时初始值
    t0_count++;
    if (t0_count >= 1000)
    {
        t0_count = 0;
    }
}
*/