#include <8052.h>
#include "Delay.h"
#include "Timer0.h"

__sbit __at (0xA5) BUZZER;
unsigned int freq_table[] = {
    63628, 63731, 63835, 63928, 64021, 64103, 64185, 64260, 64331, 64400, 64463, 64524, 
    64580, 64633, 64684, 64732, 64777, 64820, 64860, 64898, 64934, 64968, 65000, 65030, 
    65058, 65085, 65110, 65134, 65157, 65178, 65198, 65217, 65235, 65252, 65268, 65283
};
__code unsigned char  music[] = {
    5, 4,
    5, 2,
    12, 4,
    12, 4,
    11, 4,
    9, 4,
    11, 4 + 4,
    7, 4,
    7, 2,
    11, 4,
    11, 4,
    12, 4,
    7, 4,
    9, 8,
    5, 4,
    5, 2,
    7, 4,
    9, 4,
    7, 4,
    7, 4,
    5, 8,
    2, 8,
    4, 8};
unsigned char freq_select, music_select;

void main()
{
    Timer0Init();
	while (1)
	{
        freq_select = music[music_select];
		music_select++;
        Delay(125 * music[music_select]);
        music_select++;
        TR0 = 0;
        Delay(5);
        TR0 = 1;
	}
}

void Timer0_Rountine(void) __interrupt 1
{
    static unsigned int t0_count;
    TL0 = freq_table[freq_select] % 256;		//设置定时初始值
    TH0 = freq_table[freq_select] / 256;		//设置定时初始值
    BUZZER = !BUZZER;  // 1us中断一次，则buzzer频率500Hz
}