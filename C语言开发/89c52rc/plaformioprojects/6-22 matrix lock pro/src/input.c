#include "LCD1602.h"
#include "MatrixKey.h"

unsigned int count;
unsigned char key_num;

unsigned int input() {
    unsigned int password, count;
    while (1) {
        key_num = MatrixKey();
        if (key_num) {
            // 如果S1 ~ S10按键按下，输入密码
            if (key_num <= 10) {  // 1 ~ 10定义为密码区
                // 控制最多输入4位
                if (count < 4) {
                    // 密码左移1位
                    password *= 10;
                    password += key_num % 10;
                    count++;
                }
                LCD_ShowNum(2, 1, password, 4);  // 更新显示
            }
            // 如果S11按键按下，退格
            if (key_num == 11 && count > 0) {  // 判断无符号整型count防止溢出
                count--;
                password -= (password % 10);
                password /= 10;
                LCD_ShowNum(2, 1, password, 4);
            }
            // S12为确认键，并且限制使用其他返回键
            if (key_num == 12) {
                count = 0;
                return password;
            }
        }
    }
}