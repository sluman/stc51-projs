#include <8052.h>
#include "Delay.h"
#include "LCD1602.h"
#include "MatrixKey.h"
#include "input.h"

extern unsigned char key_num;
extern unsigned int count;

void main() {
    unsigned int admin, password;
    LCD_Init();
    LCD_ShowString(1, 1, "MatrixKey:");
RESET:
    LCD_ShowString(1, 12, "ADMIN");
    // 设置密码
    admin = input();
    // Delay(1000);
    LCD_ShowString(1, 12, "     ");
    LCD_ShowNum(2, 1, 0, 4);
    // 输入密码
    password = input();
    while (1) {
        if (password == admin) {
            LCD_ShowString(1, 12, "RIGHT");
            // 复位
            while (MatrixKey() != 13) {}
            LCD_ShowNum(2, 1, 0, 4);
            count = 0;
            goto RESET;
        } else {
            LCD_ShowString(1, 12, "WRONG");
            Delay(1500);
            LCD_ShowString(1, 12, "AGAIN");
            password = 0;
            count = 0;
            LCD_ShowNum(2, 1, password, 4);  // 更新显示
            password = input();
        }
    }
}