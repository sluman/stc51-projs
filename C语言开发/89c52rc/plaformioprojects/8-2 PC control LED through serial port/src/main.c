#include <8052.h>
#include "Delay.h"
#include "UART.h"

//void UART_Routine(void) __interrupt (4);

void main()
{
    UART_init();
    while (1) {}
}

void UART_Routine(void) __interrupt (4)
{
    // 区分发送和接收
    if (RI == 1)  // 接收：串行接收到停止位的中间时刻由内部硬件置位
    {
        P2 = ~SBUF;  // 接收中断，读出数据 取反使LED表示数据
        UART_sendbyte(SBUF);  // 发回数据，注意此处中断中写了主函数就不能再写了，会导致函数的重入
        RI = 0;  // 由软件复位
    }
}