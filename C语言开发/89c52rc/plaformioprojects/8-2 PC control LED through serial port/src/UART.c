#include <8052.h>

/*
 * @brief  串口初始化，4800bps@11.0592MHz
 * @param  无
 * @retval 无
 */
void UART_init()  // 4800bps@11.0592MHz
{
    SCON = 0x50;    // 0101 0000
    PCON |= 0x80;   //使能波特率倍速位SMOD
    TMOD &= 0x0F;   //清除定时器1模式位 0000 1111
    TMOD |= 0x20;   //设定定时器1为8位自动重装方式 0000 0010
    TL1 = 0xF4;		//设置定时初始值
    TH1 = 0xF4;		//设置定时重载值
    ET1 = 0;		//禁止定时器%d中断
    TR1 = 1;		//定时器1开始计时
    EA = 1;         // 启动所有中断
    ES = 1;         // 启动串口中断
}

/*
 * @brief  串口发送一个字节数据
 * @param  byte 要发送的一个字节数据
 * @retval 无
 */
void UART_sendbyte(unsigned char byte)
{
    SBUF = byte;
    while (TI == 0) {}
    TI = 0;  // 使用软件复位
}

/* 串口中断函数模板
void UART_Routine(void) __interrupt(4)
{
    if (RI == 1)
    {

        RI = 0;
    }
}
*/