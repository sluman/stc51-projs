#ifndef __UART_H__
#define __UART_H__

void UART_init();
void UART_sendbyte(unsigned char byte);

#endif