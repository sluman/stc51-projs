#include <REGX52.H>
#include "LCD1602.h"
#include "DS1302.h"
#include "Delay.h"
#include "Timer0.h"
#include "Key.h"

char key_num, mode, time_setSelect, time_setFlashFlag;

void time_show(void)
{
    DS1302_readtime();
    LCD_ShowNum(1, 1, DS1302_time[0], 2);
    LCD_ShowNum(1, 4, DS1302_time[1], 2);
    LCD_ShowNum(1, 7, DS1302_time[2], 2);
    LCD_ShowNum(2, 1, DS1302_time[3], 2);
    LCD_ShowNum(2, 4, DS1302_time[4], 2);
    LCD_ShowNum(2, 7, DS1302_time[5], 2);
}

void time_set(void)
{
    if (key_num == 2)
    {
        time_setSelect++;
//        if (time_setSelect > 5)
//        {
//            time_setSelect = 0;
//        }
        time_setSelect %= 6;  // 代替写法
    }
    if (key_num == 3)
    {
        DS1302_time[time_setSelect]++;
        // 越界判断
        // 对年判断
        if (DS1302_time[0] > 99)
        {
            DS1302_time[0] = 0;
        }
        // 对月判断
        if (DS1302_time[1] > 12)
        {
            DS1302_time[1] = 1;
        }
        // 对日判断
        if (DS1302_time[1] == 1 || DS1302_time[1] == 3 || DS1302_time[1] == 5 ||
            DS1302_time[1] == 7 || DS1302_time[1] == 8 || DS1302_time[1] == 10 ||
            DS1302_time[1] == 12)
        {
            if (DS1302_time[2] > 31)
            {
                DS1302_time[2] = 1;
            }
        }
        else if (DS1302_time[1] == 4 || DS1302_time[1] == 6 || DS1302_time[1] == 9 ||
            DS1302_time[1] == 11)
        {
            if (DS1302_time[2] > 30)
            {
                DS1302_time[2] = 1;
            }
        }
        else if (DS1302_time[1] == 2)
        {
            if (DS1302_time[0] % 4 == 0)
            {
                if (DS1302_time[2] > 29)
                {
                    DS1302_time[2] = 1;
                }
            }
            else
            {
                if (DS1302_time[2] > 28)
                {
                    DS1302_time[2] = 1;
                }
            }
        }
        if (DS1302_time[3] > 23)
        {
            DS1302_time[3] = 0;
        }
        if (DS1302_time[4] > 59)
        {
            DS1302_time[4] = 0;
        }
        if (DS1302_time[5] > 59)
        {
            DS1302_time[5] = 0;
        }
    }
    if (key_num == 4)
    {
        DS1302_time[time_setSelect]--;
        // 越界判断
        // 对年判断
        if (DS1302_time[0] < 0)
        {
            DS1302_time[0] = 99;
        }
        // 对月判断
        if (DS1302_time[1] < 1)
        {
            DS1302_time[1] = 12;
        }
        // 对日判断
        if (DS1302_time[1] == 1 || DS1302_time[1] == 3 || DS1302_time[1] == 5 ||
            DS1302_time[1] == 7 || DS1302_time[1] == 8 || DS1302_time[1] == 10 ||
            DS1302_time[1] == 12)
        {
            if (DS1302_time[2] < 1)
            {
                DS1302_time[2] = 31;
            }
            if (DS1302_time[2] > 31)
            {
                DS1302_time[2] = 1;
            }
        }
        else if (DS1302_time[1] == 4 || DS1302_time[1] == 6 || DS1302_time[1] == 9 ||
            DS1302_time[1] == 11)
        {
            if (DS1302_time[2] < 1)
            {
                DS1302_time[2] = 30;
            }
            // 补充上界
            if (DS1302_time[2] > 30)
            {
                DS1302_time[2] = 1;
            }
        }
        else if (DS1302_time[1] == 2)
        {
            if (DS1302_time[0] % 4 == 0)
            {
                if (DS1302_time[2] < 1)
                {
                    DS1302_time[2] = 29;
                }
                if (DS1302_time[2] > 29)
                {
                    DS1302_time[2] = 1;
                }
            }
            else
            {
                if (DS1302_time[2] < 1)
                {
                    DS1302_time[2] = 28;
                }
                if (DS1302_time[2] > 28)
                {
                    DS1302_time[2] = 1;
                }
            }
        }
        if (DS1302_time[3] < 0)
        {
            DS1302_time[3] = 23;
        }
        if (DS1302_time[4] < 0)
        {
            DS1302_time[4] = 59;
        }
        if (DS1302_time[5] < 0)
        {
            DS1302_time[5] = 59;
        }
    }
    if (time_setSelect == 0 && time_setFlashFlag == 1)
    {
        LCD_ShowString(1, 1, "  ");
    }
    else
    {
        LCD_ShowNum(1, 1, DS1302_time[0], 2);
    }
    if (time_setSelect == 1 && time_setFlashFlag == 1)
    {
        LCD_ShowString(1, 4, "  ");
    }
    else
    {
        LCD_ShowNum(1, 4, DS1302_time[1], 2);
    }
    if (time_setSelect == 2 && time_setFlashFlag == 1)
    {
        LCD_ShowString(1, 7, "  ");
    }
    else
    {
        LCD_ShowNum(1, 7, DS1302_time[2], 2);
    }
    if (time_setSelect == 3 && time_setFlashFlag == 1)
    {
        LCD_ShowString(2, 1, "  ");
    }
    else
    {
        LCD_ShowNum(2, 1, DS1302_time[3], 2);
    }
    if (time_setSelect == 4 && time_setFlashFlag == 1)
    {
        LCD_ShowString(2, 4, "  ");
    }
    else
    {
        LCD_ShowNum(2, 4, DS1302_time[4], 2);
    }
    if (time_setSelect == 5 && time_setFlashFlag == 1)
    {
        LCD_ShowString(2, 7, "  ");
    }
    else
    {
        LCD_ShowNum(2, 7, DS1302_time[5], 2);
    }
    LCD_ShowNum(2, 10, time_setSelect, 2);
    LCD_ShowNum(2, 13, time_setFlashFlag, 2);
}

void main()
{   
    LCD_Init();
    DS1302_init();
    Timer0Init();
    LCD_ShowString(1, 1, "  -  -  ");
    LCD_ShowString(2, 1, "  :  :  ");
    
    DS1302_settime();
    
	while (1)
    {
        key_num = Key();
        if (key_num == 1)
        {
            if (mode == 0)
            {
                mode = 1;
                time_setSelect = 0;
            }
            else if (mode == 1)
            {
                mode = 0;
                DS1302_seettime();
            }
        }
        switch (mode)
        {
            case 0: 
                time_show();
                break;
            case 1: 
                time_set();
                break;
        }
    }
}

void Timer0_Rountine(void) interrupt 1
{
    static unsigned int t0_count;
    TL0 = 0x18;		//设置定时初始值
    TH0 = 0xFC;		//设置定时初始值
    t0_count++;
    if (t0_count >= 500)
    {
        t0_count = 0;
        time_setFlashFlag = !time_setFlashFlag;  // 注意逻辑取反!和按位取反~
    }
}