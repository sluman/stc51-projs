#ifndef __DS1302_H__
#define __DS1302_H__

extern char DS1302_time[];

void DS1302_init(void);
void DS1302_writebyte(unsigned char command, Data);
unsigned char DS1302_readbyte(unsigned char command);
void DS1302_settime();
void DS1302_readtime();

#endif