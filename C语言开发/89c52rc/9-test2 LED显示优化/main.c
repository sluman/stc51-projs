#include <REG51.H>
typedef unsigned int u16;
typedef unsigned char u8;

sbit SRCLK = P3^6;         // 移位寄存器时钟输入
sbit RCK = P3^5;           // 存储寄存器时钟输入
sbit SER = P3^4;           // 串行数据输入
#define LEDDZ_COL_PORT P0  // 点阵列控制端口
u8 led_buf[8] = {0x00, 0x7c, 0x82, 0x82, 0x82, 0x7c, 0x00, 0x00};

void delay(u16 time)
{
    while(time--);
}

// 寄存器移动一位并输出
void shift(void)
{
    RCK = 0;
    RCK = 1;
    SRCLK = 0;
    SRCLK = 1;
}

void main()
{
    u8 i = 0;
    while (1)
    {
        SER = 1;  // 第1位填入1
        shift();  // 移动1位
        SER = 0;  // 之后都是0
        for (i = 0; i < 8; i++)
        {
            LEDDZ_COL_PORT = 0xff;  // 消影
            shift();  // 将1后移一位
            LEDDZ_COL_PORT = ~led_buf[i];  // LED显示对应行
            delay(1);  // 增加亮度
        }
    }
}