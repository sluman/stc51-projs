/**
 ****************************************************************************************************
 * @file        Delay.c
 * @author      榴莲派工作室(LLP STUDIO)SVP Peng
 * @version     V1.0
 * @date        2023-09-03
 * @brief       延时函数
 ****************************************************************************************************
 */
 
#include <intrins.h>

/**
 * @brief       延时500 ms（由STC-ISP生成）
 * @param       无
 * @retval      无
 */
void Delay500ms()       //@24.000MHz
{
    unsigned char data i, j, k;

    _nop_();
    _nop_();
    i = 46;
    j = 153;
    k = 245;
    do
    {
        do
        {
            while (--k);
        }
        while (--j);
    }
    while (--i);
}
