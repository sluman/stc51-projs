/**
 ****************************************************************************************************
 * @file        main.c
 * @author      榴莲派工作室(LLP STUDIO)SVP Peng
 * @version     V1.0
 * @date        2023-09-03
 * @brief       LED1 - 8流水灯
 ****************************************************************************************************
 */

#include "STC8H.H"
#include <INTRINS.H>
#include "Delay.h"

unsigned char temp;

int main()
{
    // P6口推挽输出
    P6M1 = 0X00;
    P6M0 = 0XFF;
    temp = 0XFE;  // LED1亮
    P6 = temp;
    while (1)
    {
        temp = _crol_(temp, 1);
        P6 = temp;
        Delay500ms();
    }
}
