# STC单片机开发

## 介绍
包含51单片机（STC89C52RC, STC8H8K64U）KeilC语言、汇编开发以及platformio开发

## 硬件说明
STC89C52RC使用普中开发板  
STC8H8K64U使用天问开发板

## 软件说明
Keil和platformio

## License
MIT
LLP STUDIO - SVP Peter

