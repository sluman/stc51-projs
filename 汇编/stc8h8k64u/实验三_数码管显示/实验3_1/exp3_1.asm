; *******************************************************************
; * exp3_1.asm
; * LLP STUDIO Chen Zhepeng
; * V1.1
; * 2023-11-17 (Updated)
; * 共阳数码管从左到右稳定显示12345678
; *******************************************************************
; * V1.1, 2023-11-17: 关闭了LED点阵的显示
; * V1.0, 2023-09-03: Initial version.
; *******************************************************************

P4  DATA    0C0H
P6  DATA    0E8H
P4M1 DATA   0B3H
P4M0 DATA   0B4H
P6M1 DATA   0CBH
P6M0 DATA   0CCH
HC595_SI  BIT P4.4  ; 74HC595串行输入端
HC595_SCK BIT P4.2  ; 74HC595移位时钟输入
HC595_RCK BIT P4.3  ; 74HC595锁存时钟输入

ORG 0000H
LJMP Main
ORG 0100H
Main:
; P4口P4.1推挽，其他准双向
MOV P4M1, #00H
MOV P4M0, #01H
; P4口推挽输出，从MCU输出控制HC595
;MOV P4M1, #00H
;MOV P4M0, #0FFH
; 设置P6口为推挽输出
MOV P6M1, #00H
MOV P6M0, #0FFH
MOV SP, #80H
MOV R0, #0  ; 位码计数器
MOV R1, #1  ; 段码计数器
SETB P4.0  ; 关闭LED显示
MainLoop:
LCALL ScanDisp
LCALL DELAY1MS
LJMP MainLoop

PosTable:
DB 80H, 40H, 20H, 10H, 08H, 04H, 02H, 01H
SegTable:
DB 0XC0, 0XF9, 0XA4, 0XB0, 0X99, 0X92, 0X82, 0XF8, 0X80, 0X90

; * @brief       向74HC595D发送一个字节
; * @param       ACC: 要发送的字节数据（存放在累加器中）
; * @retval      无
Hc595SendByte:
PUSH 02H
MOV R2, #8
Hc595SendLoop:
RLC A
MOV HC595_SI, C
SETB HC595_SCK
CLR HC595_SCK
DJNZ R2, Hc595SendLoop
POP 02H
RET

; * @brief       数码管动态显示
; * @param       无
; * @retval      无
ScanDisp:
MOV A, #00H
LCALL Hc595SendByte  ; 关闭点阵
MOV DPTR, #PosTable
MOV A, R0
MOVC A, @A + DPTR
LCALL Hc595SendByte  ; 输出位码
CLR HC595_RCK
NOP
SETB HC595_RCK  ; 上升沿并行锁存到输出端
NOP
CLR HC595_RCK  ; 锁存输出数据
MOV DPTR, #SegTable
MOV A, R1
MOVC A, @A + DPTR
MOV P6, A  ; 输出段码
CJNE R0, #7, Increase  ; 如果R0不等于7，跳转到Increase
MOV R0, #0
MOV R1, #1
SJMP Renew
Increase:
INC R0
INC R1
Renew:
RET

; * @brief       1 ms延时（由STC-ISP生成）
; * @param       无
; * @retval      无
DELAY1MS:			;@24.000MHz
	PUSH	30H
	PUSH	31H
	MOV		30H,#32
	MOV		31H,#39
NEXT:
	DJNZ	31H,NEXT
	DJNZ	30H,NEXT
	POP		31H
	POP		30H
	RET

END
