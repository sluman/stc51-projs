; *******************************************************************
; * exp5_3.asm
; * LLP STUDIO Chen Zhepeng
; * V1.0
; * 2023-11-24
; * T0工作在方式2，采用定时器中断方式，每计满T0引脚（P3.4）上的8个脉冲，
; * 使CMP指示灯（由P4.1的高电平点亮）闪烁2次
; *******************************************************************


P4         DATA 0C0H
P3M1       DATA    0B1H
P3M0       DATA    0B2H
P4M1       DATA 0B3H
P4M0       DATA 0B4H
AUXR       DATA 8EH     ; 辅助寄存器1
INTCLKO    DATA 8FH     ; 中断与时钟输出控制寄存器

ORG 0000H
LJMP Main
ORG 000BH
LJMP Timer0Isr
ORG 0100H
Main:
; P3.4口高阻输入，其他准双向
MOV P3M0, #00H
MOV P3M1, #10H
; P4.1口推挽，其他高阻输入
MOV P4M0, #02H
MOV P4M1, #0FDH
MOV SP, #80H
CLR P4.1
CLR TR0
SETB ET0
SETB PT0
MOV TMOD, #06H  ; 计数器0方式2：8位自动重载模式
MOV INTCLKO, #00H  ; 关闭时钟输出
;MOV AUXR, #00H  ; 12T模式：定时器定时脉冲来自系统时钟12分频 == 1 MHz
; 记录8个下降沿
MOV TH0, #0F8H
MOV TL0, #0F8H
SETB TR0
SETB EA
SJMP $

; * @brief       计数器0中断函数
; * @param       无
; * @retval      无
Timer0Isr:
MOV R2, #04H
MOV TH0, #0F8H
MOV TL0, #0F8H
Timer0LedLoop:
CPL P4.1
LCALL DELAY500MS
DJNZ R2, Timer0LedLoop
RETI

; * @brief       500 ms延时（由STC-ISP生成）
; * @param       无
; * @retval      无
DELAY500MS:			;@24.000MHz
	NOP
	NOP
	PUSH	30H
	PUSH	31H
	PUSH	32H
	MOV		30H,#61
	MOV		31H,#225
	MOV		32H,#60
NEXT:
	DJNZ	32H,NEXT
	DJNZ	31H,NEXT
	DJNZ	30H,NEXT
	POP		32H
	POP		31H
	POP		30H
	RET

END
