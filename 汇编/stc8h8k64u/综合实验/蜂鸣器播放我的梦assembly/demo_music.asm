STACK_POINTER EQU 0D0H
SEGA		BIT		P6.0
SEGB		BIT		P6.1
SEGC		BIT		P6.2
SEGD		BIT		P6.3
SEGE		BIT		P6.4
SEGF		BIT		P6.5
SEGG		BIT		P6.6
SEGDOP		BIT		P6.7
	
BUZZER		BIT		P0.0
	
DIS_BLACK	EQU		10H
P0M1		DATA		0x93
P0M0		DATA		0x94
P1M1		DATA		0x91
P1M0		DATA		0x92
P2M1		DATA		0x95
P2M0		DATA		0x96
P3M1		DATA		0xB1
P3M0		DATA		0xB2
P4M1		DATA		0xB3
P4M0		DATA		0xB4
P5M1		DATA		0xC9
P5M0		DATA		0xCA
P6M1		DATA		0xCB
P6M0		DATA		0xCC
P7M1		DATA		0xE1
P7M0		DATA		0xE2
AUXR		DATA		8EH
P4			DATA		0C0H
P5			DATA		0C8H
P6			DATA		0E8H
P7			DATA		0F8H
	
; SFR定义
P_SW2		DATA		0BAH	;最高位EAXFR=1,扩展RAM区XSFR访问控制控制位
IRTRIM		DATA		09FH
	
; 系统时钟SFR定义	
CKSEL		EQU			0FE00H	;时钟选择控制			
CLKDIV		EQU			0FE01H  ;时钟分频器
HIRCCR		EQU			0FE02H
XOSCCR		EQU			0FE03H
IRC32KCR	EQU			0FE04H
	
; IO口定义			
P_HC595_SER		BIT		P4.4
P_HC595_SRCLK	BIT		P4.2
P_HC595_RCLK	BIT		P4.3
	
; 使用变量定义			
NUMLED8				DATA		30H
display_index	 	DATA		38H
counter1				DATA		39H
counter1H			DATA		3AH
counter1L			DATA		3BH
IO_KeyState			DATA		3CH	   ; IO行列键状态变量
KeyCode				DATA		3DH	   ; 给用户使用的键码为0~15。


; 主程序
ORG		0000H
LJMP	F_Main
ORG		0100H
	CLR		A
MOV		P0M1,A
MOV		P0M0,#01H ;P0.0为推挽输出
MOV		P1M1,A
MOV		P1M0,A
MOV		P2M1,A
MOV		P2M0,A
MOV		P3M1,A
MOV		P3M0,A
MOV		P4M1,A
MOV		P4M0,#01H; P4.0为推挽输出
MOV		P5M1,A
MOV		P5M0,A
MOV		P6M1,A
MOV		P6M0,A
MOV		P7M1,A
MOV		P7M0,A
MOV		SP, #STACK_POINTER
MOV		PSW, #00H
SETB	P4.0		; 关闭LED灯显示

;系统时钟分频器初始化
MOV		P_SW2, #80H		;EAXFR=1,扩展RAM区XSFR访问控制控制位
MOV		A, #00H;  选择内部IRC24MHz
MOV		DPTR, #CKSEL
MOVX	@DPTR,A

MOV		A,#02H; 时钟2分频后为系统时钟SYSCLK=12MHz
MOV		DPTR,#CLKDIV
MOVX	@DPTR,A
MOV		P_SW2,#00H 	; 禁止访问扩展RAM区的特殊功能寄存器XFR

MOV		AUXR, #00H; 定时器定时脉冲来自系统时钟12分频 
;ORL			AUXR, #80H；定时器定时脉冲来自系统时钟不分频 

; 初始化程序
MOV		display_index,#0
MOV		R0, #NUMLED8
MOV		R2, #8
L_ClearLoop:
			MOV		@R0, #DIS_BLACK
			INC			R0
			DJNZ		R2,L_ClearLoop
			LCALL	F_DisplayScan
			
			MOV		NUMLED8, #00H
			MOV		NUMLED8+1,#00H
			
			MOV		counter1,#00H
			MOV		R7, #00;		蜂鸣器鸣叫时间置初值
			
			CLR		A
			MOV		IO_KeyState, A
			MOV		KeyCode, #0FFH	   ;键码赋初值，非01H或02H即可。设为#0FFH
										;键码编号为 0~9有效。
			
			CLR		BUZZER		;关闭蜂鸣器
			
			;主循环
L_Main_Loop: MOV	R2, #250
			MOV		NUMLED8, #01H
			MOV		NUMLED8+1, #00H
L_Main1:	LCALL	F_DisplayScan
			LCALL	F_Delay_1ms
			DJNZ	R2,L_Main1 
			LCALL	BirthDaySong
			NOP
			NOP
			
			MOV	R2, #250
			MOV		NUMLED8, #02H
			MOV		NUMLED8+1, #00H
L_Main2:	LCALL	F_DisplayScan
			LCALL	F_Delay_1ms
			DJNZ	R2,L_Main2 
			LCALL	Twotiger
			NOP
			NOP
			
			MOV	R2, #250
			MOV		NUMLED8, #03H
			MOV		NUMLED8+1, #00H
L_Main3:	LCALL	F_DisplayScan
			LCALL	F_Delay_1ms
			DJNZ	R2,L_Main3 
			LCALL	HappyNewYear
			NOP
			NOP
			
			MOV	R2, #250
			MOV		NUMLED8, #04H
			MOV		NUMLED8+1, #00H
L_Main4:	LCALL	F_DisplayScan
			LCALL	F_Delay_1ms
			DJNZ	R2,L_Main4 
			LCALL	WodezhongguoxinSong
			NOP
			NOP
			
			
			
			
			LCALL	F_Delay_1ms
			LJMP	L_Main_Loop
			
			LJMP	L_Main_Loop
			
MusicCdoMid1:	MOV	R5, #01H
				MOV	R6, #05H
				CLR	C
LOOP500do:		SETB	BUZZER
				LCALL	F_Delay_CdoMid
				CLR		BUZZER
				LCALL	F_Delay_CdoMid
				MOV		A, R6
				SUBB	A, #01H
				MOV		R6, A
				MOV		A, R5
				SUBB	A, #00H
				MOV		R5, A
				JZ		XXX10
				SJMP	LOOP500do
XXX10:			MOV		A, R6
				JNZ		LOOP500do
				RET
			

T_Display:
			DB	3FH,06H,5BH,4FH,66H,6DH,7DH,07H,7FH
			DB	6FH,77H,7CH,39H,5EH,79H,71H
			DB	00H,40H,76H,1EH,70H,38H,37H,5CH,73H
			DB	3EH,78H,3DH,67H,50H,37H,6EH

T_COM: 		DB	01H,02H,04H,08H,10H,20H,40H,80H
	
			; 函数 F_Send_595
			
F_Send_595: 	PUSH	02H
			MOV		R2,#8
L_Send_595_Loop:
			RLC		A
			MOV		P_HC595_SER, C
			SETB	P_HC595_SRCLK
			CLR		P_HC595_SRCLK
			DJNZ	R2, L_Send_595_Loop
			POP		02H
			RET
			
			; 函数F_DisplayScan
F_DisplayScan:
			PUSH	DPH
			PUSH	DPL
			PUSH	00H
			CLR		A
			LCALL	F_Send_595
			MOV		DPTR, #T_COM
			MOV		A, display_index
			MOVC	A, @A+DPTR
			NOP
			LCALL	F_Send_595
			NOP
			SETB	P_HC595_RCLK
			CLR		P_HC595_RCLK
			MOV		DPTR, #T_Display
			MOV		A, display_index
			ADD		A,#NUMLED8
			MOV		R0, A
			MOV		A,@R0
			MOVC	A, @A+DPTR
			CPL		A
			MOV		P6, A
			INC		display_index
			MOV		A,display_index
			ANL 	A, #0F8H
			JZ		L_QuitDisplayScan
			MOV		display_index, #0
L_QuitDisplayScan:	POP		00H
			POP		DPL
			POP		DPH
			RET
			

			;延时子程序（下载频率为24.000MHz）
F_Delay_1ms:PUSH	03H
			PUSH	04H
			MOV	R3,#40
			MOV	R4,#249
NEXT:		DJNZ	R4, NEXT
			DJNZ	R3, NEXT
			NOP
			NOP
			POP		04H
			POP		03H
			RET
			
			;C调音准程序段
F_Delay_Cdolow:
			MOV	R3,#30
NEXT1:	MOV	R4,#254
			DJNZ	R4,$
			DJNZ	R3, NEXT1
			RET						
F_Delay_Crelow:
			MOV	R3,#27
NEXT2:	MOV	R4,#251
			DJNZ	R4,$
			DJNZ	R3, NEXT2
			RET				
F_Delay_Cmilow:
			MOV	R3,#88
NEXT3:	MOV	R4,#68
			DJNZ	R4,$
			DJNZ	R3, NEXT3
			RET							
F_Delay_Cfalow:
			MOV	R3,#83
NEXT4:	MOV	R4,#68
			DJNZ	R4,$
			DJNZ	R3, NEXT4
			RET				
F_Delay_Csollow:
			MOV	R3,#74
NEXT5:	MOV	R4,#68
			DJNZ	R4,$
			DJNZ	R3, NEXT5
			RET				
F_Delay_Clalow:
			MOV	R3,#45
NEXT6:	MOV	R4,#100
			DJNZ	R4,$
			DJNZ	R3, NEXT6
			RET				
F_Delay_Csilow:
			MOV	R3,#40
NEXT7:	MOV	R4,#100
			DJNZ	R4,$
			DJNZ	R3, NEXT7
			RET				
			
F_Delay_CdoMid:
			MOV	R3,#39
NEXT8:	MOV	R4,#97
			DJNZ	R4,$
			DJNZ	R3, NEXT8
			RET										
F_Delay_CreMid:
			MOV		R3,#34
NEXT9:	MOV		R4,#99
			DJNZ	R4,$
			DJNZ	R3, NEXT9
			RET				
F_Delay_CmiMid:
			MOV	R3,#31
NEXT10:	MOV	R4,#97
			DJNZ	R4,$
			DJNZ	R3, NEXT10
			RET							
F_Delay_CfaMid:
			MOV	R3,#28
NEXT11:	MOV	R4,#101
			DJNZ	R4,$
			DJNZ	R3, NEXT11
			RET				
F_Delay_CsolMid:
			MOV	R3,#25
NEXT12:	MOV	R4,#101
			DJNZ	R4,$
			DJNZ	R3, NEXT12
			RET				
F_Delay_ClaMid:
			MOV	R3,#23
NEXT13:	MOV	R4,#98
			DJNZ	R4,$
			DJNZ	R3, NEXT13
			RET				
F_Delay_CsiMid:
			MOV	R3,#20
NEXT14:	MOV	R4,#100
			DJNZ	R4,$
			DJNZ	R3, NEXT14
			RET							
			
F_Delay_Cdohigh:
			MOV	R3,#21
NEXT15:	MOV	R4,#90
			DJNZ	R4,$
			DJNZ	R3, NEXT15
			RET										
F_Delay_Crehigh:
			MOV	R3,#17
NEXT16:	MOV	R4,#99
			DJNZ	R4,$
			DJNZ	R3, NEXT16
			RET				
F_Delay_Cmihigh:
			MOV	R3,#17
NEXT17:	MOV	R4,#88
			DJNZ	R4,$
			DJNZ	R3, NEXT17
			RET							
F_Delay_Cfahigh:
			MOV	R3,#16
NEXT18:	MOV	R4,#88
			DJNZ	R4,$
			DJNZ	R3, NEXT18
			RET				
F_Delay_Csolhigh:
			MOV	R3,#13
NEXT19:	MOV	R4,#97
			DJNZ	R4,$
			DJNZ	R3, NEXT19
			RET				
F_Delay_Clahigh:
			MOV	R3,#14
NEXT20:	MOV	R4,#80
			DJNZ	R4,$
			DJNZ	R3, NEXT20
			RET				
F_Delay_Csihigh:
			MOV	R3,#13
NEXT21:	MOV	R4,#77
			DJNZ	R4,$
			DJNZ	R3, NEXT21
			RET	
			
			; C调低音节音符程序
MusicCstopfourteen:	
			MOV	R7, #04
Stop:		LCALL	F_Delay_1ms
			DJNZ	R7, Stop
			RET
MusicCstopfourth:				;休止符10ms
			MOV	R7, #04
Stop3:	LCALL	F_Delay_1ms
			DJNZ	R7, Stop3
			RET


MusicCstopsixteen:	
			MOV		R7, #02
Stop2:	LCALL	F_Delay_1ms
			DJNZ	R7, Stop2
			RET
			
MusicCstophalf:				;休止符200ms
			MOV	R7, #78
Stop1:	LCALL	F_Delay_1ms
			DJNZ	R7, Stop1
			RET

MusicCdolow:MOV	R7, #157		;C调低音do四分音符（一拍）
;（时间为600ms）
Cdolow:		SETB	BUZZER
			LCALL	F_Delay_Cdolow
			CLR		BUZZER
			LCALL	F_Delay_Cdolow
			DJNZ	R7, Cdolow
			RET

MusicCdolowhalf:		; C调低音do八分音符（半拍）
; （时间为300ms）
			MOV	R7, #78
Cdolowhalf:	SETB	BUZZER
			LCALL	F_Delay_Cdolow
			CLR		BUZZER
			LCALL	F_Delay_Cdolow
			DJNZ	R7, Cdolowhalf
			RET

MusicCdolowfourth: ; C调低音do十六分音符（四分之一拍）
; （时间为150ms）
			MOV	R7, #39
Cdolowfourth:	SETB	BUZZER
			LCALL	F_Delay_Cdolow
			CLR		BUZZER
			LCALL	F_Delay_Cdolow
			DJNZ	R7, Cdolowfourth
			RET

MusicCrelow:MOV	R7, #176
Crelow:		SETB	BUZZER
			LCALL	F_Delay_Crelow
			CLR		BUZZER
			LCALL	F_Delay_Crelow
			DJNZ	R7, Crelow
			RET
			
MusicCrelowhalf:
			MOV	R7, #88
Crelowhalf:	SETB	BUZZER
			LCALL	F_Delay_Crelow
			CLR		BUZZER
			LCALL	F_Delay_Crelow
			DJNZ	R7, Crelowhalf
			RET			

MusicCrelowfourteen:
			MOV	R7, #44
Crelowfourteen:	SETB	BUZZER
			LCALL	F_Delay_Crelow
			CLR		BUZZER
			LCALL	F_Delay_Crelow
			DJNZ	R7, Crelowfourteen
			RET	
			
MusicCrelowfourth:
			MOV	R7, #44
Crelowfourth:	SETB	BUZZER
			LCALL	F_Delay_Crelow
			CLR		BUZZER
			LCALL	F_Delay_Crelow
			DJNZ	R7, Crelowfourth
			RET				
			
MusicCreloweighth:
			MOV	R7, #22
Creloweighth:	SETB	BUZZER
			LCALL	F_Delay_Crelow
			CLR		BUZZER
			LCALL	F_Delay_Crelow
			DJNZ	R7, Creloweighth
			RET				

MusicCmilow:MOV	R7, #198
Cmilow:		SETB	BUZZER
			LCALL	F_Delay_Cmilow
			CLR		BUZZER
			LCALL	F_Delay_Cmilow
			DJNZ	R7, Cmilow
			RET				
			
MusicCmilowhalf:	
			MOV	R7, #99
Cmilowhalf:	SETB	BUZZER
			LCALL	F_Delay_Cmilow
			CLR		BUZZER
			LCALL	F_Delay_Cmilow
			DJNZ	R7, Cmilowhalf
			RET			

MusicCmilowfourteen:	
			MOV	R7, #49
Cmilowfourteen:	SETB	BUZZER
			LCALL	F_Delay_Cmilow
			CLR		BUZZER
			LCALL	F_Delay_Cmilow
			DJNZ	R7, Cmilowfourteen
			RET	
			
MusicCmilowfourth:	
			MOV	R7, #49
Cmilowfourth:	SETB	BUZZER
			LCALL	F_Delay_Cmilow
			CLR		BUZZER
			LCALL	F_Delay_Cmilow
			DJNZ	R7, Cmilowfourth
			RET				
			
			
MusicCmiloweighth:	
			MOV	R7, #25
Cmiloweighth:	SETB	BUZZER
			LCALL	F_Delay_Cmilow
			CLR		BUZZER
			LCALL	F_Delay_Cmilow
			DJNZ	R7, Cmiloweighth
			RET

MusicCfalow:MOV	R7, #209
Cfalow:		SETB	BUZZER
			LCALL	F_Delay_Cfalow
			CLR		BUZZER
			LCALL	F_Delay_Cfalow
			DJNZ	R7, Cfalow
			RET				
			
MusicCfalowhalf:	
			MOV	R7, #105
Cfalowhalf:	SETB	BUZZER
			LCALL	F_Delay_Cfalow
			CLR		BUZZER
			LCALL	F_Delay_Cfalow
			DJNZ	R7, Cfalowhalf
			RET			
			
MusicCfalowfourteen:	
			MOV	R7, #52
Cfalowfourteen:	SETB	BUZZER
			LCALL	F_Delay_Cfalow
			CLR		BUZZER
			LCALL	F_Delay_Cfalow
			DJNZ	R7, Cfalowfourteen
			RET				

MusicCfalowfourth:	
			MOV	R7, #52
Cfalowfourth:	SETB	BUZZER
			LCALL	F_Delay_Cfalow
			CLR		BUZZER
			LCALL	F_Delay_Cfalow
			DJNZ	R7, Cfalowfourth
			RET	


MusicCfaloweighth:	
			MOV	R7, #26
Cfaloweighth:	SETB	BUZZER
			LCALL	F_Delay_Cfalow
			CLR		BUZZER
			LCALL	F_Delay_Cfalow
			DJNZ	R7, Cfaloweighth
			RET			
			
MusicCsollow:MOV	R7, #235
Csollow:	SETB	BUZZER
			LCALL	F_Delay_Csollow
			CLR		BUZZER
			LCALL	F_Delay_Csollow
			DJNZ	R7, Csollow
			RET			

MusicCsollowhalf:
			MOV	R7, #118
Csollowhalf:SETB	BUZZER
			LCALL	F_Delay_Csollow
			CLR		BUZZER
			LCALL	F_Delay_Csollow
			DJNZ	R7, Csollowhalf
			RET		

MusicCsollowfourteen:
			MOV	R7, #59
Csollowfourteen:SETB	BUZZER
			LCALL	F_Delay_Csollow
			CLR		BUZZER
			LCALL	F_Delay_Csollow
			DJNZ	R7, Csollowfourteen
			RET	

MusicCsollowfourth:
			MOV	R7, #59
Csollowfourth:SETB	BUZZER
			LCALL	F_Delay_Csollow
			CLR		BUZZER
			LCALL	F_Delay_Csollow
			DJNZ	R7, Csollowfourth
			RET	


MusicCsolloweighth:
			MOV	R7, #30
Csolloweighth:SETB	BUZZER
			LCALL	F_Delay_Csollow
			CLR		BUZZER
			LCALL	F_Delay_Csollow
			DJNZ	R7, Csolloweighth
			RET	

MusicClalow:
			MOV	R5, #01H
			MOV	R6, #08H
			CLR	C
Clalow:	SETB		BUZZER
			LCALL	F_Delay_Clalow
			CLR	BUZZER
			LCALL	F_Delay_Clalow
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX
			SJMP	Clalow
XXX:		MOV		A, R6
			JNZ		Clalow
			RET

MusicClalowhalf:	
			MOV	R7, #132
Clalowhalf:	SETB	BUZZER
			LCALL	F_Delay_Clalow
			CLR		BUZZER
			LCALL	F_Delay_Clalow
			DJNZ	R7, Clalowhalf
			RET
			
MusicClalowfourteen:	
			MOV	R7, #66
Clalowfourteen:	SETB	BUZZER
			LCALL	F_Delay_Clalow
			CLR		BUZZER
			LCALL	F_Delay_Clalow
			DJNZ	R7, Clalowfourteen
			RET

MusicClalowfourth:	
			MOV	R7, #66
Clalowfourth:	SETB	BUZZER
			LCALL	F_Delay_Clalow
			CLR		BUZZER
			LCALL	F_Delay_Clalow
			DJNZ	R7, Clalowfourth
			RET


MusicClaloweighth:	
			MOV	R7, #33
Claloweighth:	SETB	BUZZER
			LCALL	F_Delay_Clalow
			CLR		BUZZER
			LCALL	F_Delay_Clalow
			DJNZ	R7, Claloweighth
			RET			
			
MusicCsilow:MOV	R5, #01H
			MOV	R6, #28H
			CLR	C
Csilow:		SETB	BUZZER
			LCALL	F_Delay_Csilow
			CLR		BUZZER
			LCALL	F_Delay_Csilow
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX1
			SJMP	Csilow
XXX1:		MOV		A, R6
			JNZ		Csilow
			RET	

MusicCsilowhalf:
			MOV	R7, #148
Csilowhalf:	SETB	BUZZER
			LCALL	F_Delay_Csilow
			CLR		BUZZER
			LCALL	F_Delay_Csilow
			DJNZ	R7, Csilowhalf
			RET	

MusicCsilowfourteen:
			MOV	R7, #74
Csilowfourteen:	SETB	BUZZER
			LCALL	F_Delay_Csilow
			CLR		BUZZER
			LCALL	F_Delay_Csilow
			DJNZ	R7, Csilowfourteen
			RET	

MusicCsilowfourth:
			MOV	R7, #74
Csilowfourth:	SETB	BUZZER
			LCALL	F_Delay_Csilow
			CLR		BUZZER
			LCALL	F_Delay_Csilow
			DJNZ	R7, Csilowfourth
			RET	


MusicCsiloweighth:
			MOV	R7, #37
Csiloweighth:	SETB	BUZZER
			LCALL	F_Delay_Csilow
			CLR		BUZZER
			LCALL	F_Delay_Csilow
			DJNZ	R7, Csiloweighth
			RET				
			
			
			;C调中音节音符程序
MusicCdoMid:MOV	R5, #01H
			MOV	R6, #3AH
			CLR	C		
CdoMid:		SETB	BUZZER
			LCALL	F_Delay_CdoMid
			CLR		BUZZER
			LCALL	F_Delay_CdoMid
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX2
			SJMP	CdoMid
XXX2:		MOV		A, R6
			JNZ		CdoMid
			RET

MusicCdoMidhalf:
			MOV	R7, #157
CdoMidhalf:	SETB	BUZZER
			LCALL	F_Delay_CdoMid
			CLR		BUZZER
			LCALL	F_Delay_CdoMid
			DJNZ	R7, CdoMidhalf
			RET

MusicCdoMidfourteen:
			MOV	R7, #78
CdoMidfourteen:	SETB	BUZZER
			LCALL	F_Delay_CdoMid
			CLR		BUZZER
			LCALL	F_Delay_CdoMid
			DJNZ	R7, CdoMidfourteen
			RET

MusicCdoMidfourth:
			MOV	R7, #78
CdoMidfourth:	SETB	BUZZER
			LCALL	F_Delay_CdoMid
			CLR		BUZZER
			LCALL	F_Delay_CdoMid
			DJNZ	R7, CdoMidfourth
			RET

MusicCdoMideighth:
			MOV	R7, #39
CdoMideighth:	SETB	BUZZER
			LCALL	F_Delay_CdoMid
			CLR		BUZZER
			LCALL	F_Delay_CdoMid
			DJNZ	R7, CdoMideighth
			RET


MusicCreMid:MOV	R5, #01H
			MOV	R6, #60H
			CLR	C
CreMid:		SETB	BUZZER
			LCALL	F_Delay_CreMid
			CLR		BUZZER
			LCALL	F_Delay_CreMid
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX3
			SJMP	CreMid
XXX3:		MOV		A, R6
			JNZ		CreMid
			RET
			
MusicCreMidhalf:
			MOV	R7, #176
CreMidhalf:	SETB	BUZZER
			LCALL	F_Delay_CreMid
			CLR		BUZZER
			LCALL	F_Delay_CreMid
			DJNZ	R7, CreMidhalf
			RET			

MusicCreMidfourteen:
			MOV	R7, #88
CreMidfourteen:	SETB	BUZZER
			LCALL	F_Delay_CreMid
			CLR		BUZZER
			LCALL	F_Delay_CreMid
			DJNZ	R7, CreMidfourteen
			RET	

MusicCreMidfourth:
			MOV	R7, #88
CreMidfourth:	SETB	BUZZER
			LCALL	F_Delay_CreMid
			CLR		BUZZER
			LCALL	F_Delay_CreMid
			DJNZ	R7, CreMidfourth
			RET	

MusicCreMideighth:
			MOV	R7, #44
CreMideighth:	SETB	BUZZER
			LCALL	F_Delay_CreMid
			CLR		BUZZER
			LCALL	F_Delay_CreMid
			DJNZ	R7, CreMideighth
			RET				
			
			
MusicCmiMidhalf:	
			MOV	R7, #198
CmiMidhalf:	SETB	BUZZER
			LCALL	F_Delay_CmiMid
			CLR		BUZZER
			LCALL	F_Delay_CmiMid
			DJNZ	R7, CmiMidhalf
			RET			

MusicCmiMidfourteen:	
			MOV	R7, #99
CmiMidfourteen:	SETB	BUZZER
			LCALL	F_Delay_CmiMid
			CLR		BUZZER
			LCALL	F_Delay_CmiMid
			DJNZ	R7, CmiMidfourteen
			RET	

MusicCmiMidfourth:	
			MOV	R7, #99
CmiMidfourth:	SETB	BUZZER
			LCALL	F_Delay_CmiMid
			CLR		BUZZER
			LCALL	F_Delay_CmiMid
			DJNZ	R7, CmiMidfourth
			RET	

MusicCmiMideighth:	
			MOV	R7, #50
CmiMideighth:	SETB	BUZZER
			LCALL	F_Delay_CmiMid
			CLR		BUZZER
			LCALL	F_Delay_CmiMid
			DJNZ	R7, CmiMideighth
			RET				
			
			
MusicCfaMidhalf:	
			MOV	R7, #209
CfaMidhalf:	SETB	BUZZER
			LCALL	F_Delay_CfaMid
			CLR		BUZZER
			LCALL	F_Delay_CfaMid
			DJNZ	R7, CfaMidhalf
			RET			
			
MusicCfaMidfourteen:	
			MOV	R7, #105
CfaMidfourteen:	SETB	BUZZER
			LCALL	F_Delay_CfaMid
			CLR		BUZZER
			LCALL	F_Delay_CfaMid
			DJNZ	R7, CfaMidfourteen
			RET				

MusicCfaMidfourth:	
			MOV	R7, #105
CfaMidfourth:	SETB	BUZZER
			LCALL	F_Delay_CfaMid
			CLR		BUZZER
			LCALL	F_Delay_CfaMid
			DJNZ	R7, CfaMidfourth
			RET	

MusicCfaMideighth:	
			MOV	R7, #53
CfaMideighth:	SETB	BUZZER
			LCALL	F_Delay_CfaMid
			CLR		BUZZER
			LCALL	F_Delay_CfaMid
			DJNZ	R7, CfaMideighth
			RET				
			
			
MusicCsolMidhalf:
			MOV	R7, #235
CsolMidhalf:SETB	BUZZER
			LCALL	F_Delay_CsolMid
			CLR		BUZZER
			LCALL	F_Delay_CsolMid
			DJNZ	R7, CsolMidhalf
			RET		

MusicCsolMidfourteen:
			MOV	R7, #118
CsolMidfourteen:SETB	BUZZER
			LCALL	F_Delay_CsolMid
			CLR		BUZZER
			LCALL	F_Delay_CsolMid
			DJNZ	R7, CsolMidfourteen
			RET	

MusicCsolMidfourth:
			MOV	R7, #118
CsolMidfourth:SETB	BUZZER
			LCALL	F_Delay_CsolMid
			CLR		BUZZER
			LCALL	F_Delay_CsolMid
			DJNZ	R7, CsolMidfourth
			RET	

MusicCsolMideighth:
			MOV	R7, #59
CsolMideighth:SETB	BUZZER
			LCALL	F_Delay_CsolMid
			CLR		BUZZER
			LCALL	F_Delay_CsolMid
			DJNZ	R7, CsolMideighth
			RET


MusicClaMidhalf:	
			MOV	R5, #01H
			MOV	R6, #08H
			CLR	C
ClaMidhalf:	SETB	BUZZER
			LCALL	F_Delay_ClaMid
			CLR		BUZZER
			LCALL	F_Delay_ClaMid
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX4
			SJMP	ClaMidhalf
XXX4:		MOV		A, R6
			JNZ		ClaMidhalf
			RET
			
MusicClaMidfourteen:	
			MOV	R7, #132
ClaMidfourteen:	SETB	BUZZER
			LCALL	F_Delay_ClaMid
			CLR		BUZZER
			LCALL	F_Delay_ClaMid
			DJNZ	R7, ClaMidfourteen
			RET

MusicClaMidfourth:	
			MOV	R7, #132
ClaMidfourth:	SETB	BUZZER
			LCALL	F_Delay_ClaMid
			CLR		BUZZER
			LCALL	F_Delay_ClaMid
			DJNZ	R7, ClaMidfourth
			RET

MusicClaMideighth:	
			MOV	R7, #66
ClaMideighth:	SETB	BUZZER
			LCALL	F_Delay_ClaMid
			CLR		BUZZER
			LCALL	F_Delay_ClaMid
			DJNZ	R7, ClaMideighth
			RET			
						
MusicCsiMidhalf:
			MOV	R5, #01H
			MOV	R6, #28H
			CLR	C
CsiMidhalf:	SETB	BUZZER
			LCALL	F_Delay_CsiMid
			CLR		BUZZER
			LCALL	F_Delay_CsiMid
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX5
			SJMP	CsiMidhalf
XXX5:		MOV		A, R6
			JNZ		CsiMidhalf
			RET	

MusicCsiMidfourteen:
			MOV	R7, #150
CsiMidfourteen:	SETB	BUZZER
			LCALL	F_Delay_CsiMid
			CLR		BUZZER
			LCALL	F_Delay_CsiMid
			DJNZ	R7, CsiMidfourteen
			RET	

MusicCsiMidfourth:
			MOV	R7, #150
CsiMidfourth:	SETB	BUZZER
			LCALL	F_Delay_CsiMid
			CLR		BUZZER
			LCALL	F_Delay_CsiMid
			DJNZ	R7, CsiMidfourth
			RET	

MusicCsiMideighth:
			MOV	R7, #75
CsiMideighth:	SETB	BUZZER
			LCALL	F_Delay_CsiMid
			CLR		BUZZER
			LCALL	F_Delay_CsiMid
			DJNZ	R7, CsiMideighth
			RET				
			
			
		; C调高音节音符程序

MusicCdohighhalf:
			MOV	R5, #01H
			MOV	R6, #3AH
			CLR	C
Cdohighhalf:SETB	BUZZER
			LCALL	F_Delay_Cdohigh
			CLR		BUZZER
			LCALL	F_Delay_Cdohigh
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX6
			SJMP	Cdohighhalf
XXX6:		MOV		A, R6
			JNZ		Cdohighhalf
			RET

MusicCdohighfourteen:
			MOV	R7, #157
Cdohighfourteen:	SETB	BUZZER
			LCALL	F_Delay_Cdohigh
			CLR		BUZZER
			LCALL	F_Delay_Cdohigh
			DJNZ	R7, Cdohighfourteen
			RET

MusicCdohighfourth:
			MOV	R7, #157
Cdohighfourth:	SETB	BUZZER
			LCALL	F_Delay_Cdohigh
			CLR		BUZZER
			LCALL	F_Delay_Cdohigh
			DJNZ	R7, Cdohighfourth
			RET


MusicCdohigheighth:
			MOV	R7, #79
Cdohigheighth:	SETB	BUZZER
			LCALL	F_Delay_Cdohigh
			CLR		BUZZER
			LCALL	F_Delay_Cdohigh
			DJNZ	R7, Cdohigheighth
			RET			
						
MusicCrehighhalf:
			MOV	R5, #01H
			MOV	R6, #60H
			CLR	C
Crehighhalf:	SETB	BUZZER
			LCALL	F_Delay_Crehigh
			CLR		BUZZER
			LCALL	F_Delay_Crehigh
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX7
			SJMP	Crehighhalf
XXX7:		MOV		A, R6
			JNZ		Crehighhalf
			RET			

MusicCrehighfourteen:
			MOV	R7, #176
Crehighfourteen:	SETB	BUZZER
			LCALL	F_Delay_Crehigh
			CLR		BUZZER
			LCALL	F_Delay_Crehigh
			DJNZ	R7, Crehighfourteen
			RET		

MusicCrehighfourth:
			MOV	R7, #176
Crehighfourth:	SETB	BUZZER
			LCALL	F_Delay_Crehigh
			CLR		BUZZER
			LCALL	F_Delay_Crehigh
			DJNZ	R7, Crehighfourth
			RET	

MusicCrehigheighth:
			MOV	R7, #88
Crehigheighth:	SETB	BUZZER
			LCALL	F_Delay_Crehigh
			CLR		BUZZER
			LCALL	F_Delay_Crehigh
			DJNZ	R7, Crehigheighth
			RET	

MusicCmihighfourteen:	
			MOV	R7, #198
Cmihighfourteen:	SETB	BUZZER
			LCALL	F_Delay_Cmihigh
			CLR		BUZZER
			LCALL	F_Delay_Cmihigh
			DJNZ	R7, Cmihighfourteen
			RET	

MusicCmihighfourth:	
			MOV	R7, #198
Cmihighfourth:	SETB	BUZZER
			LCALL	F_Delay_Cmihigh
			CLR		BUZZER
			LCALL	F_Delay_Cmihigh
			DJNZ	R7, Cmihighfourth
			RET	

MusicCmihigheighth:	
			MOV	R7, #99
Cmihigheighth:	SETB	BUZZER
			LCALL	F_Delay_Cmihigh
			CLR		BUZZER
			LCALL	F_Delay_Cmihigh
			DJNZ	R7, Cmihigheighth
			RET	

MusicCfahighfourteen:	
			MOV	R7, #210
Cfahighfourteen:	SETB	BUZZER
			LCALL	F_Delay_Cfahigh
			CLR		BUZZER
			LCALL	F_Delay_Cfahigh
			DJNZ	R7, Cfahighfourteen
			RET				

MusicCfahighfourth:	
			MOV	R7, #210
Cfahighfourth:	SETB	BUZZER
			LCALL	F_Delay_Cfahigh
			CLR		BUZZER
			LCALL	F_Delay_Cfahigh
			DJNZ	R7, Cfahighfourth
			RET	

MusicCfahigheighth:	
			MOV	R7, #105
Cfahigheighth:	SETB	BUZZER
			LCALL	F_Delay_Cfahigh
			CLR		BUZZER
			LCALL	F_Delay_Cfahigh
			DJNZ	R7, Cfahigheighth
			RET				
			
			
MusicCsolhighfourteen:
			MOV		R7, #235
Csolhighfourteen:SETB	BUZZER
			LCALL	F_Delay_Csolhigh
			CLR		BUZZER
			LCALL	F_Delay_Csolhigh
			DJNZ	R7, Csolhighfourteen
			RET	

MusicCsolhighfourth:
			MOV		R7, #235
Csolhighfourth:SETB	BUZZER
			LCALL	F_Delay_Csolhigh
			CLR		BUZZER
			LCALL	F_Delay_Csolhigh
			DJNZ	R7, Csolhighfourth
			RET	

MusicCsolhigheighth:
			MOV		R7, #117
Csolhigheighth:SETB	BUZZER
			LCALL	F_Delay_Csolhigh
			CLR		BUZZER
			LCALL	F_Delay_Csolhigh
			DJNZ	R7, Csolhigheighth
			RET				
			
			
MusicClahighfourteen:	
			MOV	R5, #01H
			MOV	R6, #08H
			CLR	C
Clahighfourteen:	SETB	BUZZER
			LCALL	F_Delay_Clahigh
			CLR		BUZZER
			LCALL	F_Delay_Clahigh
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX8
			SJMP	Clahighfourteen
XXX8:		MOV		A, R6
			JNZ		Clahighfourteen
			RET

MusicClahighfourth:	
			MOV	R5, #01H
			MOV	R6, #08H
			CLR	C
Clahighfourth:	SETB	BUZZER
			LCALL	F_Delay_Clahigh
			CLR		BUZZER
			LCALL	F_Delay_Clahigh
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX81
			SJMP	Clahighfourth
XXX81:		MOV		A, R6
			JNZ		Clahighfourth
			RET

MusicClahigheighth:
			MOV		R7, #132
Clahigheighth:SETB	BUZZER
			LCALL	F_Delay_Clahigh
			CLR		BUZZER
			LCALL	F_Delay_Clahigh
			DJNZ	R7, Clahigheighth
			RET				
			
MusicCsihighfourteen:
			MOV	R5, #01H
			MOV	R6, #28H
			CLR	C
Csihighfourteen:	SETB	BUZZER
			LCALL	F_Delay_Csihigh
			CLR		BUZZER
			LCALL	F_Delay_Csihigh
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX9
			SJMP	Csihighfourteen
XXX9:		MOV		A, R6
			JNZ		Csihighfourteen
			RET	

MusicCsihighfourth:
			MOV	R5, #01H
			MOV	R6, #28H
			CLR	C
Csihighfourth:	SETB	BUZZER
			LCALL	F_Delay_Csihigh
			CLR		BUZZER
			LCALL	F_Delay_Csihigh
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX91
			SJMP	Csihighfourth
XXX91:		MOV		A, R6
			JNZ		Csihighfourth
			RET	

MusicCsihigheighth:
			MOV		R7, #148
Csihigheighth:SETB	BUZZER
			LCALL	F_Delay_Csihigh
			CLR		BUZZER
			LCALL	F_Delay_Csihigh
			DJNZ	R7, Csihigheighth
			RET

			; 生日歌子程序
BirthDaySong: 
			MOV		R2, #02H
Birthday: 	LCALL	MusicCsollowhalf
			LCALL	MusicCstopsixteen
			LCALL	MusicCsollowhalf
			LCALL	MusicClalow
			LCALL	MusicCsollow
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			
			LCALL	MusicCdoMid
			LCALL	MusicCsilow
			LCALL	MusicCsilow
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			
			LCALL	MusicCsollowhalf
			LCALL	MusicCsollowhalf
			LCALL	MusicClalow
			LCALL	MusicCsollow
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			
			LCALL	MusicCreMid
			LCALL	MusicCdoMid
			LCALL	MusicCdoMid
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			
			LCALL	MusicCsollowhalf
			LCALL	MusicCstopsixteen
			LCALL	MusicCsollowhalf
			LCALL	MusicCsolMidhalf
			LCALL	MusicCsolMidhalf 
			LCALL	MusicCmiMidhalf
			LCALL	MusicCmiMidhalf
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			
			LCALL	MusicCdoMid
			LCALL	MusicCsilow
			LCALL	MusicClalow
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			
			LCALL	MusicCfaMidhalf
			LCALL	MusicCfaMidhalf
			LCALL	MusicCmiMidhalf
			LCALL	MusicCmiMidhalf
			LCALL	MusicCdoMid
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			
			LCALL	MusicCreMid
			LCALL	MusicCdoMid
			LCALL	MusicCdoMid
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			DJNZ	R2, Birthday1
			RET
Birthday1: LJMP		Birthday

;  
			; 两只老虎儿歌
			
Twotiger: 	MOV	R2, #02H
Twotiger1:	LCALL	MusicCdoMid
			LCALL	MusicCreMid
			LCALL	MusicCmiMidhalf
			LCALL	MusicCmiMidhalf
			LCALL	MusicCdoMid
			LCALL	MusicCstopfourteen
			
			LCALL	MusicCdoMid
			LCALL	MusicCreMid
			LCALL	MusicCmiMidhalf
			LCALL	MusicCmiMidhalf
			LCALL	MusicCdoMid
			LCALL	MusicCstopfourteen

			LCALL	MusicCmiMidhalf
			LCALL	MusicCmiMidhalf
			LCALL	MusicCfaMidhalf
			LCALL	MusicCfaMidhalf
			LCALL	MusicCsolMidhalf
			LCALL	MusicCsolMidhalf
			LCALL	MusicCsolMidhalf
			LCALL	MusicCsolMidhalf
			LCALL	MusicCstopfourteen
			
			LCALL	MusicCmiMidhalf
			LCALL	MusicCmiMidhalf
			LCALL	MusicCfaMidhalf
			LCALL	MusicCfaMidhalf
			LCALL	MusicCsolMidhalf
			LCALL	MusicCsolMidhalf
			LCALL	MusicCsolMidhalf
			LCALL	MusicCsolMidhalf
			LCALL	MusicCstopfourteen
			
			LCALL	MusicCsolMidhalf
			LCALL	MusicClaMidhalf
			LCALL	MusicCsolMidhalf
			LCALL	MusicCfaMidhalf
			LCALL	MusicCmiMidhalf
			LCALL	MusicCmiMidhalf
			LCALL	MusicCdoMid
			
			LCALL	MusicCsolMidhalf
			LCALL	MusicClaMidhalf
			LCALL	MusicCsolMidhalf
			LCALL	MusicCfaMidhalf
			LCALL	MusicCmiMidhalf
			LCALL	MusicCmiMidhalf
			LCALL	MusicCdoMid
			LCALL	MusicCstopfourteen
			
			LCALL	MusicCdoMid
			LCALL	MusicCsollow
			LCALL	MusicCdoMid
			LCALL	MusicCdoMid
			LCALL	MusicCstopfourteen
			
			LCALL	MusicCdoMid
			LCALL	MusicCsollow
			LCALL	MusicCdoMid
			LCALL	MusicCdoMid
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			LCALL	MusicCstopfourteen
			DJNZ	R2,Twotiger2
			RET
Twotiger2:	LJMP	Twotiger1		
;  
			;新年好儿歌
HappyNewYear:	MOV	R2, #02H
HappyNewYear1:	LCALL	MusicCdoMidhalf
				LCALL	MusicCstopsixteen
				LCALL	MusicCdoMidhalf
				LCALL	MusicCstopsixteen
				LCALL	MusicCdoMid
				LCALL	MusicCsollow
				LCALL	MusicCstopfourteen
				
				LCALL	MusicCmiMidhalf
				LCALL	MusicCstopsixteen
				LCALL	MusicCmiMidhalf
				LCALL	MusicCstopsixteen
				LCALL	MusicCmiMidhalf
				LCALL	MusicCmiMidhalf
				LCALL	MusicCdoMid
				LCALL	MusicCstopfourteen
				
				LCALL	MusicCdoMidhalf
				LCALL	MusicCmiMidhalf
				LCALL	MusicCsolMidhalf
				LCALL	MusicCsolMidhalf
				LCALL	MusicCstopsixteen
				LCALL	MusicCsolMidhalf
				LCALL	MusicCsolMidhalf
				
				LCALL	MusicCfaMidhalf
				LCALL	MusicCmiMidhalf
				LCALL	MusicCreMid
				LCALL	MusicCreMid
				LCALL	MusicCstopfourteen
				
				LCALL	MusicCreMidhalf
				LCALL	MusicCmiMidhalf
				LCALL	MusicCfaMidhalf
				LCALL	MusicCfaMidhalf
				LCALL	MusicCstopsixteen
				LCALL	MusicCfaMidhalf
				LCALL	MusicCfaMidhalf
				LCALL	MusicCstopfourteen
				
				LCALL	MusicCmiMidhalf
				LCALL	MusicCreMidhalf
				LCALL	MusicCmiMidhalf
				LCALL	MusicCmiMidhalf
				LCALL	MusicCdoMid
				LCALL	MusicCstopfourteen
				
				LCALL	MusicCdoMidhalf
				LCALL	MusicCmiMidhalf
				LCALL	MusicCreMid 
				LCALL	MusicCsollow
				
				LCALL	MusicCsilowhalf
				LCALL	MusicCreMidhalf
				LCALL	MusicCdoMid 
				LCALL	MusicCdoMid 
				LCALL	MusicCstopfourteen
				LCALL	MusicCstopfourteen
				LCALL	MusicCstopfourteen
				LCALL	MusicCstopfourteen
				LCALL	MusicCstopfourteen
				LCALL	MusicCstopfourteen
				LCALL	MusicCstopfourteen
				LCALL	MusicCstopfourteen
				LCALL	MusicCstopfourteen
				LCALL	MusicCstopfourteen
				LCALL	MusicCstopfourteen
				LCALL	MusicCstopfourteen
				DJNZ	R2, HappyNewYear2
				RET
HappyNewYear2:	LJMP	HappyNewYear1			
				
			
			
			END
