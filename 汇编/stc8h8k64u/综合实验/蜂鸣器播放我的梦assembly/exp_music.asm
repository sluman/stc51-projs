; *******************************************************************
; * exp_music.asm
; * LLP STUDIO Chen Zhepeng
; * V1.0
; * 2023-11-29 (Updated)
; * 蜂鸣器播放音乐
; *******************************************************************
; * V1.1, 2023-11-29: 修改了《我的梦》歌曲乐谱
; * V1.0, 2023-11-28: Initial version.
; *******************************************************************

BUZZER BIT P0.0
P4                    DATA  0C0H
P6                    DATA  0E8H
P0M1                  DATA  093H
P0M0                  DATA  094H
P4M1                  DATA  0B3H
P4M0                  DATA  0B4H
P6M1                  DATA  0CBH
P6M0                  DATA  0CCH
HC595_SI              BIT   P4.4       ; 74HC595串行输入端
HC595_SCK             BIT   P4.2       ; 74HC595移位时钟输入
HC595_RCK             BIT   P4.3       ; 74HC595锁存时钟输入
NIXIE8                DATA  30H        ; 显示缓冲30H ~ 37H
DISP_BLACK            EQU   10H        ; 对应段码表0FFH（全黑）
AUXR                  DATA  08EH       
INTCLKO               DATA  8FH        ; 中断与时钟输出控制寄存器
P_SW2                 DATA  0BAH       ; 外设端口切换寄存器2
IRTRIM                DATA  09FH       ; IRC频率调整寄存器
CKSEL                 EQU   0FE00H     ; 时钟选择寄存器
CLKDIV                EQU   0FE01H     ; 时钟分频寄存器
HIRCCR                EQU   0FE02H     ; 内部高速振荡器控制寄存器
XOSCCR                EQU   0FE03H     ; 外部晶振控制寄存器
IRC32KCR              EQU   0FE04H     ; 内部32K振荡器控制寄存器
disp_index            DATA  38H        ; 显示位索引
counter1 DATA 39H
counter1_h DATA 3AH
counter1_l DATA 3BH

ORG 0000H
LJMP Main
ORG 0100H
Main:
; P0.0为推挽输出
MOV P0M1, #00H
MOV P0M0, #01H
; P4.0设置成推挽输出，其余为准双向口
MOV P4M1, #00H
MOV P4M0, #01H
; P6设置为推挽输出
MOV P6M1, #00H
MOV P6M0, #0FFH
MOV SP, #80H
MOV PSW, #0
SETB P4.0  ; 关闭8个LED
; 系统时钟分频器初始化
MOV P_SW2, #80H  ; 最高位EAXFR == 1，使能扩展RAM区特殊功能寄存器XFR访问
; 选择内部高速高精度IRC（24 MHz）
MOV A, #00H
MOV DPTR, #CKSEL
MOVX @DPTR, A
; 时钟2分频后为系统时钟SYSCLK == 12 MHz
MOV A, #02H
MOV DPTR, #CLKDIV
MOVX @DPTR, A
MOV P_SW2, #00H
MOV AUXR, #00H  ; 定时器定时脉冲来自系统时钟12分频
MOV disp_index, #0  ; 消隐
MOV R0, #NIXIE8  ; 灭灯
MOV R2, #8  ; 8个数码管，执行8次
ClearLoop:
MOV @R0, #DISP_BLACK
INC R0
DJNZ R2, ClearLoop
LCALL ScanDisp
MOV NIXIE8, #00H
MOV NIXIE8 + 1, #00H
MOV counter1, #00H
MOV R7, #00  ; 蜂鸣器鸣叫时间置初值
CLR BUZZER  ; 关闭蜂鸣器
MainLoop:
MOV R2, #250
MOV NIXIE8, #01H
MOV NIXIE8 + 1, #00H
Main1:
LCALL ScanDisp
LCALL DELAY1MS
DJNZ R2, Main1
LCALL MyDreamSong
NOP
NOP

LCALL DELAY1MS
LJMP MainLoop

MusicCdoMid1:
MOV R5, #01H
MOV R6, #05H
CLR C
LOOP500do:
SETB BUZZER
LCALL F_Delay_CdoMid
CLR BUZZER
LCALL F_Delay_CdoMid
MOV		A, R6
SUBB	A, #01H
MOV		R6, A
MOV		A, R5
SUBB	A, #00H
MOV		R5, A
JZ		XXX10
SJMP	LOOP500do
XXX10:			MOV		A, R6
				JNZ		LOOP500do
				RET


PosTable:
DB 01H, 02H, 04H, 08H, 10H, 20H, 40H, 80H
SegTable:
DB 0XC0, 0XF9, 0XA4, 0XB0, 0X99, 0X92, 0X82, 0XF8
DB 0X80, 0X90, 0X88, 0X83, 0XC6, 0XA1, 0X86, 0X8E
DB 0FFH  ; 全黑
DB 0X40, 0X79, 0X24, 0X30, 0X19, 0X12, 0X02, 0X78
DB 0X00, 0X10, 0X08, 0X03, 0X46, 0X21, 0X06, 0X0E
DB 0XFF


; * @brief       向74HC595D发送一个字节
; * @param       ACC: 要发送的字节数据（存放在累加器中）
; * @retval      无
Hc595SendByte:
PUSH 02H
MOV R2, #8
Hc595SendLoop:
RLC A
MOV HC595_SI, C
SETB HC595_SCK
CLR HC595_SCK
DJNZ R2, Hc595SendLoop
POP 02H
RET


; * @brief       数码管动态显示
; * @param       无
; * @retval      无
ScanDisp:
PUSH DPH
PUSH DPL
PUSH 00H
MOV A, #00H
LCALL Hc595SendByte  ; 输出点阵位码，关闭点阵
MOV DPTR, #PosTable
MOV A, disp_index
MOVC A, @A + DPTR
LCALL Hc595SendByte  ; 输出位码
CLR HC595_RCK
NOP
SETB HC595_RCK  ; 上升沿并行锁存到输出端
NOP
CLR HC595_RCK  ; 锁存输出数据
MOV DPTR, #SegTable
MOV A, disp_index
ADD A, #NIXIE8  ; 根据disp_index指示的第几个数码管，
MOV R0, A  ; 定位要显示的数码管
MOV A, @R0
MOVC A, @A + DPTR
MOV P6, A  ; 输出段码
INC disp_index
MOV A, disp_index
ANL A, #0F8H
JZ QuitScanDisp  ; disp_index == 0 ~ 7跳转
; if (disp_index >= 8)
; {
MOV disp_index, #0  ; 8位结束回0
; }
; else
; {
QuitScanDisp:
POP 00H
POP DPL
POP DPH
RET
; }

; * @brief       1 ms延时（由STC-ISP生成）
; * @param       无
; * @retval      无
DELAY1MS:			;@12.000MHz
	NOP
	NOP
	PUSH	30H
	PUSH	31H
	MOV		30H,#16
	MOV		31H,#145
NEXT:
	DJNZ	31H,NEXT
	DJNZ	30H,NEXT
	POP		31H
	POP		30H
	RET


;C调音准程序段
F_Delay_Cdolow:
			MOV	R3,#30
NEXT1:	MOV	R4,#254
			DJNZ	R4,$
			DJNZ	R3, NEXT1
			RET						
F_Delay_Crelow:
			MOV	R3,#27
NEXT2:	MOV	R4,#251
			DJNZ	R4,$
			DJNZ	R3, NEXT2
			RET				
F_Delay_Cmilow:
			MOV	R3,#88
NEXT3:	MOV	R4,#68
			DJNZ	R4,$
			DJNZ	R3, NEXT3
			RET							
F_Delay_Cfalow:
			MOV	R3,#83
NEXT4:	MOV	R4,#68
			DJNZ	R4,$
			DJNZ	R3, NEXT4
			RET				
F_Delay_Csollow:
			MOV	R3,#74
NEXT5:	MOV	R4,#68
			DJNZ	R4,$
			DJNZ	R3, NEXT5
			RET				
F_Delay_Clalow:
			MOV	R3,#45
NEXT6:	MOV	R4,#100
			DJNZ	R4,$
			DJNZ	R3, NEXT6
			RET				
F_Delay_Csilow:
			MOV	R3,#40
NEXT7:	MOV	R4,#100
			DJNZ	R4,$
			DJNZ	R3, NEXT7
			RET				
			
F_Delay_CdoMid:
			MOV	R3,#39
NEXT8:	MOV	R4,#97
			DJNZ	R4,$
			DJNZ	R3, NEXT8
			RET										
F_Delay_CreMid:
			MOV		R3,#34
NEXT9:	MOV		R4,#99
			DJNZ	R4,$
			DJNZ	R3, NEXT9
			RET				
F_Delay_CmiMid:
			MOV	R3,#31
NEXT10:	MOV	R4,#97
			DJNZ	R4,$
			DJNZ	R3, NEXT10
			RET							
F_Delay_CfaMid:
			MOV	R3,#28
NEXT11:	MOV	R4,#101
			DJNZ	R4,$
			DJNZ	R3, NEXT11
			RET				
F_Delay_CsolMid:
			MOV	R3,#25
NEXT12:	MOV	R4,#101
			DJNZ	R4,$
			DJNZ	R3, NEXT12
			RET				
F_Delay_ClaMid:
			MOV	R3,#23
NEXT13:	MOV	R4,#98
			DJNZ	R4,$
			DJNZ	R3, NEXT13
			RET				
F_Delay_CsiMid:
			MOV	R3,#20
NEXT14:	MOV	R4,#100
			DJNZ	R4,$
			DJNZ	R3, NEXT14
			RET							
			
F_Delay_Cdohigh:
			MOV	R3,#21
NEXT15:	MOV	R4,#90
			DJNZ	R4,$
			DJNZ	R3, NEXT15
			RET										
F_Delay_Crehigh:
			MOV	R3,#17
NEXT16:	MOV	R4,#99
			DJNZ	R4,$
			DJNZ	R3, NEXT16
			RET				
F_Delay_Cmihigh:
			MOV	R3,#17
NEXT17:	MOV	R4,#88
			DJNZ	R4,$
			DJNZ	R3, NEXT17
			RET							
F_Delay_Cfahigh:
			MOV	R3,#16
NEXT18:	MOV	R4,#88
			DJNZ	R4,$
			DJNZ	R3, NEXT18
			RET				
F_Delay_Csolhigh:
			MOV	R3,#13
NEXT19:	MOV	R4,#97
			DJNZ	R4,$
			DJNZ	R3, NEXT19
			RET				
F_Delay_Clahigh:
			MOV	R3,#14
NEXT20:	MOV	R4,#80
			DJNZ	R4,$
			DJNZ	R3, NEXT20
			RET				
F_Delay_Csihigh:
			MOV	R3,#13
NEXT21:	MOV	R4,#77
			DJNZ	R4,$
			DJNZ	R3, NEXT21
			RET	
			
			; C调低音节音符程序
MusicCstopfourteen:	
			MOV	R7, #04
Stop:		LCALL	DELAY1MS
			DJNZ	R7, Stop
			RET
MusicCstopfourth:				;休止符10ms
			MOV	R7, #04
Stop3:	LCALL	DELAY1MS
			DJNZ	R7, Stop3
			RET


MusicCstopsixteen:	
			MOV		R7, #02
Stop2:	LCALL	DELAY1MS
			DJNZ	R7, Stop2
			RET
			
MusicCstophalf:				;休止符200ms
			MOV	R7, #78
Stop1:	LCALL	DELAY1MS
			DJNZ	R7, Stop1
			RET

MusicCdolow:MOV	R7, #157		;C调低音do四分音符（一拍）
;（时间为600ms）
Cdolow:		SETB	BUZZER
			LCALL	F_Delay_Cdolow
			CLR		BUZZER
			LCALL	F_Delay_Cdolow
			DJNZ	R7, Cdolow
			RET

MusicCdolowhalf:		; C调低音do八分音符（半拍）
; （时间为300ms）
			MOV	R7, #78
Cdolowhalf:	SETB	BUZZER
			LCALL	F_Delay_Cdolow
			CLR		BUZZER
			LCALL	F_Delay_Cdolow
			DJNZ	R7, Cdolowhalf
			RET

MusicCdolowfourth: ; C调低音do十六分音符（四分之一拍）
; （时间为150ms）
			MOV	R7, #39
Cdolowfourth:	SETB	BUZZER
			LCALL	F_Delay_Cdolow
			CLR		BUZZER
			LCALL	F_Delay_Cdolow
			DJNZ	R7, Cdolowfourth
			RET

MusicCrelow:MOV	R7, #176
Crelow:		SETB	BUZZER
			LCALL	F_Delay_Crelow
			CLR		BUZZER
			LCALL	F_Delay_Crelow
			DJNZ	R7, Crelow
			RET
			
MusicCrelowhalf:
			MOV	R7, #88
Crelowhalf:	SETB	BUZZER
			LCALL	F_Delay_Crelow
			CLR		BUZZER
			LCALL	F_Delay_Crelow
			DJNZ	R7, Crelowhalf
			RET			

MusicCrelowfourteen:
			MOV	R7, #44
Crelowfourteen:	SETB	BUZZER
			LCALL	F_Delay_Crelow
			CLR		BUZZER
			LCALL	F_Delay_Crelow
			DJNZ	R7, Crelowfourteen
			RET	
			
MusicCrelowfourth:
			MOV	R7, #44
Crelowfourth:	SETB	BUZZER
			LCALL	F_Delay_Crelow
			CLR		BUZZER
			LCALL	F_Delay_Crelow
			DJNZ	R7, Crelowfourth
			RET				
			
MusicCreloweighth:
			MOV	R7, #22
Creloweighth:	SETB	BUZZER
			LCALL	F_Delay_Crelow
			CLR		BUZZER
			LCALL	F_Delay_Crelow
			DJNZ	R7, Creloweighth
			RET				

MusicCmilow:MOV	R7, #198
Cmilow:		SETB	BUZZER
			LCALL	F_Delay_Cmilow
			CLR		BUZZER
			LCALL	F_Delay_Cmilow
			DJNZ	R7, Cmilow
			RET				
			
MusicCmilowhalf:	
			MOV	R7, #99
Cmilowhalf:	SETB	BUZZER
			LCALL	F_Delay_Cmilow
			CLR		BUZZER
			LCALL	F_Delay_Cmilow
			DJNZ	R7, Cmilowhalf
			RET			

MusicCmilowfourteen:	
			MOV	R7, #49
Cmilowfourteen:	SETB	BUZZER
			LCALL	F_Delay_Cmilow
			CLR		BUZZER
			LCALL	F_Delay_Cmilow
			DJNZ	R7, Cmilowfourteen
			RET	
			
MusicCmilowfourth:	
			MOV	R7, #49
Cmilowfourth:	SETB	BUZZER
			LCALL	F_Delay_Cmilow
			CLR		BUZZER
			LCALL	F_Delay_Cmilow
			DJNZ	R7, Cmilowfourth
			RET				
			
			
MusicCmiloweighth:	
			MOV	R7, #25
Cmiloweighth:	SETB	BUZZER
			LCALL	F_Delay_Cmilow
			CLR		BUZZER
			LCALL	F_Delay_Cmilow
			DJNZ	R7, Cmiloweighth
			RET

MusicCfalow:MOV	R7, #209
Cfalow:		SETB	BUZZER
			LCALL	F_Delay_Cfalow
			CLR		BUZZER
			LCALL	F_Delay_Cfalow
			DJNZ	R7, Cfalow
			RET				
			
MusicCfalowhalf:	
			MOV	R7, #105
Cfalowhalf:	SETB	BUZZER
			LCALL	F_Delay_Cfalow
			CLR		BUZZER
			LCALL	F_Delay_Cfalow
			DJNZ	R7, Cfalowhalf
			RET			
			
MusicCfalowfourteen:	
			MOV	R7, #52
Cfalowfourteen:	SETB	BUZZER
			LCALL	F_Delay_Cfalow
			CLR		BUZZER
			LCALL	F_Delay_Cfalow
			DJNZ	R7, Cfalowfourteen
			RET				

MusicCfalowfourth:	
			MOV	R7, #52
Cfalowfourth:	SETB	BUZZER
			LCALL	F_Delay_Cfalow
			CLR		BUZZER
			LCALL	F_Delay_Cfalow
			DJNZ	R7, Cfalowfourth
			RET	


MusicCfaloweighth:	
			MOV	R7, #26
Cfaloweighth:	SETB	BUZZER
			LCALL	F_Delay_Cfalow
			CLR		BUZZER
			LCALL	F_Delay_Cfalow
			DJNZ	R7, Cfaloweighth
			RET			
			
MusicCsollow:MOV	R7, #235
Csollow:	SETB	BUZZER
			LCALL	F_Delay_Csollow
			CLR		BUZZER
			LCALL	F_Delay_Csollow
			DJNZ	R7, Csollow
			RET			

MusicCsollowhalf:
			MOV	R7, #118
Csollowhalf:SETB	BUZZER
			LCALL	F_Delay_Csollow
			CLR		BUZZER
			LCALL	F_Delay_Csollow
			DJNZ	R7, Csollowhalf
			RET		

MusicCsollowfourteen:
			MOV	R7, #59
Csollowfourteen:SETB	BUZZER
			LCALL	F_Delay_Csollow
			CLR		BUZZER
			LCALL	F_Delay_Csollow
			DJNZ	R7, Csollowfourteen
			RET	

MusicCsollowfourth:
			MOV	R7, #59
Csollowfourth:SETB	BUZZER
			LCALL	F_Delay_Csollow
			CLR		BUZZER
			LCALL	F_Delay_Csollow
			DJNZ	R7, Csollowfourth
			RET	


MusicCsolloweighth:
			MOV	R7, #30
Csolloweighth:SETB	BUZZER
			LCALL	F_Delay_Csollow
			CLR		BUZZER
			LCALL	F_Delay_Csollow
			DJNZ	R7, Csolloweighth
			RET	

MusicClalow:
			MOV	R5, #01H
			MOV	R6, #08H
			CLR	C
Clalow:	SETB		BUZZER
			LCALL	F_Delay_Clalow
			CLR	BUZZER
			LCALL	F_Delay_Clalow
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX
			SJMP	Clalow
XXX:		MOV		A, R6
			JNZ		Clalow
			RET

MusicClalowhalf:	
			MOV	R7, #132
Clalowhalf:	SETB	BUZZER
			LCALL	F_Delay_Clalow
			CLR		BUZZER
			LCALL	F_Delay_Clalow
			DJNZ	R7, Clalowhalf
			RET
			
MusicClalowfourteen:	
			MOV	R7, #66
Clalowfourteen:	SETB	BUZZER
			LCALL	F_Delay_Clalow
			CLR		BUZZER
			LCALL	F_Delay_Clalow
			DJNZ	R7, Clalowfourteen
			RET

MusicClalowfourth:	
			MOV	R7, #66
Clalowfourth:	SETB	BUZZER
			LCALL	F_Delay_Clalow
			CLR		BUZZER
			LCALL	F_Delay_Clalow
			DJNZ	R7, Clalowfourth
			RET


MusicClaloweighth:	
			MOV	R7, #33
Claloweighth:	SETB	BUZZER
			LCALL	F_Delay_Clalow
			CLR		BUZZER
			LCALL	F_Delay_Clalow
			DJNZ	R7, Claloweighth
			RET			
			
MusicCsilow:MOV	R5, #01H
			MOV	R6, #28H
			CLR	C
Csilow:		SETB	BUZZER
			LCALL	F_Delay_Csilow
			CLR		BUZZER
			LCALL	F_Delay_Csilow
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX1
			SJMP	Csilow
XXX1:		MOV		A, R6
			JNZ		Csilow
			RET	

MusicCsilowhalf:
			MOV	R7, #148
Csilowhalf:	SETB	BUZZER
			LCALL	F_Delay_Csilow
			CLR		BUZZER
			LCALL	F_Delay_Csilow
			DJNZ	R7, Csilowhalf
			RET	

MusicCsilowfourteen:
			MOV	R7, #74
Csilowfourteen:	SETB	BUZZER
			LCALL	F_Delay_Csilow
			CLR		BUZZER
			LCALL	F_Delay_Csilow
			DJNZ	R7, Csilowfourteen
			RET	

MusicCsilowfourth:
			MOV	R7, #74
Csilowfourth:	SETB	BUZZER
			LCALL	F_Delay_Csilow
			CLR		BUZZER
			LCALL	F_Delay_Csilow
			DJNZ	R7, Csilowfourth
			RET	


MusicCsiloweighth:
			MOV	R7, #37
Csiloweighth:	SETB	BUZZER
			LCALL	F_Delay_Csilow
			CLR		BUZZER
			LCALL	F_Delay_Csilow
			DJNZ	R7, Csiloweighth
			RET				
			
			
			;C调中音节音符程序
MusicCdoMid:MOV	R5, #01H
			MOV	R6, #3AH
			CLR	C		
CdoMid:		SETB	BUZZER
			LCALL	F_Delay_CdoMid
			CLR		BUZZER
			LCALL	F_Delay_CdoMid
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX2
			SJMP	CdoMid
XXX2:		MOV		A, R6
			JNZ		CdoMid
			RET

MusicCdoMidhalf:
			MOV	R7, #157
CdoMidhalf:	SETB	BUZZER
			LCALL	F_Delay_CdoMid
			CLR		BUZZER
			LCALL	F_Delay_CdoMid
			DJNZ	R7, CdoMidhalf
			RET

MusicCdoMidfourteen:
			MOV	R7, #78
CdoMidfourteen:	SETB	BUZZER
			LCALL	F_Delay_CdoMid
			CLR		BUZZER
			LCALL	F_Delay_CdoMid
			DJNZ	R7, CdoMidfourteen
			RET

MusicCdoMidfourth:
			MOV	R7, #78
CdoMidfourth:	SETB	BUZZER
			LCALL	F_Delay_CdoMid
			CLR		BUZZER
			LCALL	F_Delay_CdoMid
			DJNZ	R7, CdoMidfourth
			RET

MusicCdoMideighth:
			MOV	R7, #39
CdoMideighth:	SETB	BUZZER
			LCALL	F_Delay_CdoMid
			CLR		BUZZER
			LCALL	F_Delay_CdoMid
			DJNZ	R7, CdoMideighth
			RET


MusicCreMid:MOV	R5, #01H
			MOV	R6, #60H
			CLR	C
CreMid:		SETB	BUZZER
			LCALL	F_Delay_CreMid
			CLR		BUZZER
			LCALL	F_Delay_CreMid
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX3
			SJMP	CreMid
XXX3:		MOV		A, R6
			JNZ		CreMid
			RET
			
MusicCreMidhalf:
			MOV	R7, #176
CreMidhalf:	SETB	BUZZER
			LCALL	F_Delay_CreMid
			CLR		BUZZER
			LCALL	F_Delay_CreMid
			DJNZ	R7, CreMidhalf
			RET			

MusicCreMidfourteen:
			MOV	R7, #88
CreMidfourteen:	SETB	BUZZER
			LCALL	F_Delay_CreMid
			CLR		BUZZER
			LCALL	F_Delay_CreMid
			DJNZ	R7, CreMidfourteen
			RET	

MusicCreMidfourth:
			MOV	R7, #88
CreMidfourth:	SETB	BUZZER
			LCALL	F_Delay_CreMid
			CLR		BUZZER
			LCALL	F_Delay_CreMid
			DJNZ	R7, CreMidfourth
			RET	

MusicCreMideighth:
			MOV	R7, #44
CreMideighth:	SETB	BUZZER
			LCALL	F_Delay_CreMid
			CLR		BUZZER
			LCALL	F_Delay_CreMid
			DJNZ	R7, CreMideighth
			RET				
			
			
MusicCmiMidhalf:	
			MOV	R7, #198
CmiMidhalf:	SETB	BUZZER
			LCALL	F_Delay_CmiMid
			CLR		BUZZER
			LCALL	F_Delay_CmiMid
			DJNZ	R7, CmiMidhalf
			RET			

MusicCmiMidfourteen:	
			MOV	R7, #99
CmiMidfourteen:	SETB	BUZZER
			LCALL	F_Delay_CmiMid
			CLR		BUZZER
			LCALL	F_Delay_CmiMid
			DJNZ	R7, CmiMidfourteen
			RET	

MusicCmiMidfourth:	
			MOV	R7, #99
CmiMidfourth:	SETB	BUZZER
			LCALL	F_Delay_CmiMid
			CLR		BUZZER
			LCALL	F_Delay_CmiMid
			DJNZ	R7, CmiMidfourth
			RET	

MusicCmiMideighth:	
			MOV	R7, #50
CmiMideighth:	SETB	BUZZER
			LCALL	F_Delay_CmiMid
			CLR		BUZZER
			LCALL	F_Delay_CmiMid
			DJNZ	R7, CmiMideighth
			RET				
			
			
MusicCfaMidhalf:	
			MOV	R7, #209
CfaMidhalf:	SETB	BUZZER
			LCALL	F_Delay_CfaMid
			CLR		BUZZER
			LCALL	F_Delay_CfaMid
			DJNZ	R7, CfaMidhalf
			RET			
			
MusicCfaMidfourteen:	
			MOV	R7, #105
CfaMidfourteen:	SETB	BUZZER
			LCALL	F_Delay_CfaMid
			CLR		BUZZER
			LCALL	F_Delay_CfaMid
			DJNZ	R7, CfaMidfourteen
			RET				

MusicCfaMidfourth:	
			MOV	R7, #105
CfaMidfourth:	SETB	BUZZER
			LCALL	F_Delay_CfaMid
			CLR		BUZZER
			LCALL	F_Delay_CfaMid
			DJNZ	R7, CfaMidfourth
			RET	

MusicCfaMideighth:	
			MOV	R7, #53
CfaMideighth:	SETB	BUZZER
			LCALL	F_Delay_CfaMid
			CLR		BUZZER
			LCALL	F_Delay_CfaMid
			DJNZ	R7, CfaMideighth
			RET				
			
			
MusicCsolMidhalf:
			MOV	R7, #235
CsolMidhalf:SETB	BUZZER
			LCALL	F_Delay_CsolMid
			CLR		BUZZER
			LCALL	F_Delay_CsolMid
			DJNZ	R7, CsolMidhalf
			RET		

MusicCsolMidfourteen:
			MOV	R7, #118
CsolMidfourteen:SETB	BUZZER
			LCALL	F_Delay_CsolMid
			CLR		BUZZER
			LCALL	F_Delay_CsolMid
			DJNZ	R7, CsolMidfourteen
			RET	

MusicCsolMidfourth:
			MOV	R7, #118
CsolMidfourth:SETB	BUZZER
			LCALL	F_Delay_CsolMid
			CLR		BUZZER
			LCALL	F_Delay_CsolMid
			DJNZ	R7, CsolMidfourth
			RET	

MusicCsolMideighth:
			MOV	R7, #59
CsolMideighth:SETB	BUZZER
			LCALL	F_Delay_CsolMid
			CLR		BUZZER
			LCALL	F_Delay_CsolMid
			DJNZ	R7, CsolMideighth
			RET


MusicClaMidhalf:	
			MOV	R5, #01H
			MOV	R6, #08H
			CLR	C
ClaMidhalf:	SETB	BUZZER
			LCALL	F_Delay_ClaMid
			CLR		BUZZER
			LCALL	F_Delay_ClaMid
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX4
			SJMP	ClaMidhalf
XXX4:		MOV		A, R6
			JNZ		ClaMidhalf
			RET
			
MusicClaMidfourteen:	
			MOV	R7, #132
ClaMidfourteen:	SETB	BUZZER
			LCALL	F_Delay_ClaMid
			CLR		BUZZER
			LCALL	F_Delay_ClaMid
			DJNZ	R7, ClaMidfourteen
			RET

MusicClaMidfourth:	
			MOV	R7, #132
ClaMidfourth:	SETB	BUZZER
			LCALL	F_Delay_ClaMid
			CLR		BUZZER
			LCALL	F_Delay_ClaMid
			DJNZ	R7, ClaMidfourth
			RET

MusicClaMideighth:	
			MOV	R7, #66
ClaMideighth:	SETB	BUZZER
			LCALL	F_Delay_ClaMid
			CLR		BUZZER
			LCALL	F_Delay_ClaMid
			DJNZ	R7, ClaMideighth
			RET			
						
MusicCsiMidhalf:
			MOV	R5, #01H
			MOV	R6, #28H
			CLR	C
CsiMidhalf:	SETB	BUZZER
			LCALL	F_Delay_CsiMid
			CLR		BUZZER
			LCALL	F_Delay_CsiMid
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX5
			SJMP	CsiMidhalf
XXX5:		MOV		A, R6
			JNZ		CsiMidhalf
			RET	

MusicCsiMidfourteen:
			MOV	R7, #150
CsiMidfourteen:	SETB	BUZZER
			LCALL	F_Delay_CsiMid
			CLR		BUZZER
			LCALL	F_Delay_CsiMid
			DJNZ	R7, CsiMidfourteen
			RET	

MusicCsiMidfourth:
			MOV	R7, #150
CsiMidfourth:	SETB	BUZZER
			LCALL	F_Delay_CsiMid
			CLR		BUZZER
			LCALL	F_Delay_CsiMid
			DJNZ	R7, CsiMidfourth
			RET	

MusicCsiMideighth:
			MOV	R7, #75
CsiMideighth:	SETB	BUZZER
			LCALL	F_Delay_CsiMid
			CLR		BUZZER
			LCALL	F_Delay_CsiMid
			DJNZ	R7, CsiMideighth
			RET				
			
			
		; C调高音节音符程序

MusicCdohighhalf:
			MOV	R5, #01H
			MOV	R6, #3AH
			CLR	C
Cdohighhalf:SETB	BUZZER
			LCALL	F_Delay_Cdohigh
			CLR		BUZZER
			LCALL	F_Delay_Cdohigh
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX6
			SJMP	Cdohighhalf
XXX6:		MOV		A, R6
			JNZ		Cdohighhalf
			RET

MusicCdohighfourteen:
			MOV	R7, #157
Cdohighfourteen:	SETB	BUZZER
			LCALL	F_Delay_Cdohigh
			CLR		BUZZER
			LCALL	F_Delay_Cdohigh
			DJNZ	R7, Cdohighfourteen
			RET

MusicCdohighfourth:
			MOV	R7, #157
Cdohighfourth:	SETB	BUZZER
			LCALL	F_Delay_Cdohigh
			CLR		BUZZER
			LCALL	F_Delay_Cdohigh
			DJNZ	R7, Cdohighfourth
			RET


MusicCdohigheighth:
			MOV	R7, #79
Cdohigheighth:	SETB	BUZZER
			LCALL	F_Delay_Cdohigh
			CLR		BUZZER
			LCALL	F_Delay_Cdohigh
			DJNZ	R7, Cdohigheighth
			RET			
						
MusicCrehighhalf:
			MOV	R5, #01H
			MOV	R6, #60H
			CLR	C
Crehighhalf:	SETB	BUZZER
			LCALL	F_Delay_Crehigh
			CLR		BUZZER
			LCALL	F_Delay_Crehigh
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX7
			SJMP	Crehighhalf
XXX7:		MOV		A, R6
			JNZ		Crehighhalf
			RET			

MusicCrehighfourteen:
			MOV	R7, #176
Crehighfourteen:	SETB	BUZZER
			LCALL	F_Delay_Crehigh
			CLR		BUZZER
			LCALL	F_Delay_Crehigh
			DJNZ	R7, Crehighfourteen
			RET		

MusicCrehighfourth:
			MOV	R7, #176
Crehighfourth:	SETB	BUZZER
			LCALL	F_Delay_Crehigh
			CLR		BUZZER
			LCALL	F_Delay_Crehigh
			DJNZ	R7, Crehighfourth
			RET	

MusicCrehigheighth:
			MOV	R7, #88
Crehigheighth:	SETB	BUZZER
			LCALL	F_Delay_Crehigh
			CLR		BUZZER
			LCALL	F_Delay_Crehigh
			DJNZ	R7, Crehigheighth
			RET	

MusicCmihighfourteen:	
			MOV	R7, #198
Cmihighfourteen:	SETB	BUZZER
			LCALL	F_Delay_Cmihigh
			CLR		BUZZER
			LCALL	F_Delay_Cmihigh
			DJNZ	R7, Cmihighfourteen
			RET	

MusicCmihighfourth:	
			MOV	R7, #198
Cmihighfourth:	SETB	BUZZER
			LCALL	F_Delay_Cmihigh
			CLR		BUZZER
			LCALL	F_Delay_Cmihigh
			DJNZ	R7, Cmihighfourth
			RET	

MusicCmihigheighth:	
			MOV	R7, #99
Cmihigheighth:	SETB	BUZZER
			LCALL	F_Delay_Cmihigh
			CLR		BUZZER
			LCALL	F_Delay_Cmihigh
			DJNZ	R7, Cmihigheighth
			RET	

MusicCfahighfourteen:	
			MOV	R7, #210
Cfahighfourteen:	SETB	BUZZER
			LCALL	F_Delay_Cfahigh
			CLR		BUZZER
			LCALL	F_Delay_Cfahigh
			DJNZ	R7, Cfahighfourteen
			RET				

MusicCfahighfourth:	
			MOV	R7, #210
Cfahighfourth:	SETB	BUZZER
			LCALL	F_Delay_Cfahigh
			CLR		BUZZER
			LCALL	F_Delay_Cfahigh
			DJNZ	R7, Cfahighfourth
			RET	

MusicCfahigheighth:	
			MOV	R7, #105
Cfahigheighth:	SETB	BUZZER
			LCALL	F_Delay_Cfahigh
			CLR		BUZZER
			LCALL	F_Delay_Cfahigh
			DJNZ	R7, Cfahigheighth
			RET				
			
			
MusicCsolhighfourteen:
			MOV		R7, #235
Csolhighfourteen:SETB	BUZZER
			LCALL	F_Delay_Csolhigh
			CLR		BUZZER
			LCALL	F_Delay_Csolhigh
			DJNZ	R7, Csolhighfourteen
			RET	

MusicCsolhighfourth:
			MOV		R7, #235
Csolhighfourth:SETB	BUZZER
			LCALL	F_Delay_Csolhigh
			CLR		BUZZER
			LCALL	F_Delay_Csolhigh
			DJNZ	R7, Csolhighfourth
			RET	

MusicCsolhigheighth:
			MOV		R7, #117
Csolhigheighth:SETB	BUZZER
			LCALL	F_Delay_Csolhigh
			CLR		BUZZER
			LCALL	F_Delay_Csolhigh
			DJNZ	R7, Csolhigheighth
			RET				
			
			
MusicClahighfourteen:	
			MOV	R5, #01H
			MOV	R6, #08H
			CLR	C
Clahighfourteen:	SETB	BUZZER
			LCALL	F_Delay_Clahigh
			CLR		BUZZER
			LCALL	F_Delay_Clahigh
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX8
			SJMP	Clahighfourteen
XXX8:		MOV		A, R6
			JNZ		Clahighfourteen
			RET

MusicClahighfourth:	
			MOV	R5, #01H
			MOV	R6, #08H
			CLR	C
Clahighfourth:	SETB	BUZZER
			LCALL	F_Delay_Clahigh
			CLR		BUZZER
			LCALL	F_Delay_Clahigh
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX81
			SJMP	Clahighfourth
XXX81:		MOV		A, R6
			JNZ		Clahighfourth
			RET

MusicClahigheighth:
			MOV		R7, #132
Clahigheighth:SETB	BUZZER
			LCALL	F_Delay_Clahigh
			CLR		BUZZER
			LCALL	F_Delay_Clahigh
			DJNZ	R7, Clahigheighth
			RET				
			
MusicCsihighfourteen:
			MOV	R5, #01H
			MOV	R6, #28H
			CLR	C
Csihighfourteen:	SETB	BUZZER
			LCALL	F_Delay_Csihigh
			CLR		BUZZER
			LCALL	F_Delay_Csihigh
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX9
			SJMP	Csihighfourteen
XXX9:		MOV		A, R6
			JNZ		Csihighfourteen
			RET	

MusicCsihighfourth:
			MOV	R5, #01H
			MOV	R6, #28H
			CLR	C
Csihighfourth:	SETB	BUZZER
			LCALL	F_Delay_Csihigh
			CLR		BUZZER
			LCALL	F_Delay_Csihigh
			MOV		A, R6
			SUBB	A, #01H
			MOV		R6, A
			MOV		A, R5
			SUBB	A, #00H
			MOV		R5, A
			JZ		XXX91
			SJMP	Csihighfourth
XXX91:		MOV		A, R6
			JNZ		Csihighfourth
			RET	

MusicCsihigheighth:
			MOV		R7, #148
Csihigheighth:SETB	BUZZER
			LCALL	F_Delay_Csihigh
			CLR		BUZZER
			LCALL	F_Delay_Csihigh
			DJNZ	R7, Csihigheighth
			RET

; 我的梦——张靓颖
MyDreamSong:
MOV R2, #02H
MyDream:
LCALL MusicCmiMidhalf    ; 就
LCALL MusicCsolMidhalf   ; 让
LCALL MusicClaMidhalf    ; 光
LCALL MusicClaMidhalf
LCALL MusicCstopsixteen
LCALL MusicClaMidhalf    ; 芒
LCALL MusicCstopsixteen
LCALL MusicClaMidhalf    ; 折
LCALL MusicCstopsixteen
LCALL MusicClaMidhalf    ; 射
LCALL MusicClaMidhalf
LCALL MusicCstopsixteen
LCALL MusicClaMidhalf    ; 泪
LCALL MusicClaMidhalf
LCALL MusicCsolMidhalf   ; 湿
LCALL MusicCstopfourteen
LCALL MusicCmiMidhalf    ; 的
LCALL MusicCmiMidhalf
LCALL MusicCsolMidhalf   ; 瞳
LCALL MusicCmiMidhalf    ; 孔
LCALL MusicCmiMidhalf
LCALL MusicCmiMidhalf
LCALL MusicCstopfourteen
LCALL MusicCmiMidhalf    ; 映
LCALL MusicCdoMidhalf    ; 出
LCALL MusicCreMidhalf    ; 心
LCALL MusicCreMidhalf
LCALL MusicCstopsixteen
LCALL MusicCreMidhalf    ; 中
LCALL MusicCreMidhalf
LCALL MusicCdoMidhalf    ; 最
LCALL MusicClalowhalf    ; 想
LCALL MusicClalowhalf
LCALL MusicCmiMidhalf    ; 拥
LCALL MusicCmiMidhalf
LCALL MusicCdoMidhalf    ; 有
LCALL MusicCreMidhalf    ; 的
LCALL MusicCreMidhalf
LCALL MusicCdoMidhalf    ; 彩
LCALL MusicClalowhalf    ; 虹
LCALL MusicCstopfourteen
LCALL MusicCstopfourteen
LCALL MusicCstopfourteen
LCALL MusicCstopfourteen
LCALL MusicCstopfourteen
LCALL MusicCstopfourteen
LCALL MusicCstopfourteen
LCALL MusicCstopfourteen
LCALL MusicCstopfourteen
LCALL MusicCstopfourteen
LCALL MusicCstopfourteen
LCALL MusicCstopfourteen
DJNZ R2, MyDream1
RET
MyDream1: LJMP MyDream

END