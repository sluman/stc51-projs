; ******************************************************************************
; * demo1_2.asm
; * LLP STUDIO SVP Peng
; * V1.0
; * 2023-09-02
; * LED1 - 8流水灯，每隔0.5 s点亮一个
; ******************************************************************************

P6  DATA    0E8H
P6M1 DATA   0CBH
P6M0 DATA   0CCH

ORG 0000H
; 设置P6口为推挽输出
MOV P6M0, #0FFH
MOV P6M1, #00H
MOV A, #0FEH

Loop:
MOV P6, A
RL A
CALL DELAY500MS
JMP Loop

DELAY500MS:			;@24.000MHz
	NOP
	NOP
	PUSH	30H
	PUSH	31H
	PUSH	32H
	MOV		30H,#61
	MOV		31H,#225
	MOV		32H,#60
NEXT:
	DJNZ	32H,NEXT
	DJNZ	31H,NEXT
	DJNZ	30H,NEXT
	POP		32H
	POP		31H
	POP		30H
	RET

END