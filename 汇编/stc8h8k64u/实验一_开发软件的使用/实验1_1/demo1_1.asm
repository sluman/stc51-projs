; *******************************************************************
; * demo1_2.asm
; * LLP STUDIO Chen Zhepeng
; * V1.1
; * 2023-11-12 (Updated)
; * LED-0603闪烁
; *******************************************************************
; * V1.1, 2023-11-12: 按照课程规范写Main函数跳转并添加了设置堆栈代码
; * V1.0, 2023-09-02: Initial version.
; *******************************************************************

P4   DATA   0C0H
P6   DATA   0E8H
P4M1 DATA   0B3H
P4M0 DATA   0B4H
P6M1 DATA   0CBH
P6M0 DATA   0CCH

ORG 0000H
LJMP Main
ORG 0100H
Main:
MOV SP, #80H
; 设置P4.1推挽输出，其他高阻输入
MOV P4M0, #02H
MOV P4M1, #0FDH
MainLoop:
CPL P4.1
LCALL DELAY500MS
SJMP MainLoop

DELAY500MS:         ;@24.000MHz
    NOP
    NOP
    PUSH    30H
    PUSH    31H
    PUSH    32H
    MOV     30H,#61
    MOV     31H,#225
    MOV     32H,#60
NEXT:
    DJNZ    32H,NEXT
    DJNZ    31H,NEXT
    DJNZ    30H,NEXT
    POP     32H
    POP     31H
    POP     30H
    RET
END