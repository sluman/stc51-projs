; *******************************************************************
; * exp2_1.asm
; * LLP STUDIO Chen ZhePeng
; * V1.0
; * 2023-11-10
; * 8个LED灯P6间隔0.5 s交替闪烁
; *******************************************************************

P4   DATA 0C0H
P6   DATA 0E8H
P4M1 DATA 0B3H
P4M0 DATA 0B4H
P6M1 DATA 0CBH
P6M0 DATA 0CCH
HC595_SI  BIT P4.4  ; 74HC595串行输入端
HC595_SCK BIT P4.2  ; 74HC595移位时钟输入
HC595_RCK BIT P4.3  ; 74HC595锁存时钟输入

ORG 0000H
LJMP Main
ORG 0100H
Main:
; 设置P4.0推挽，其他准双向
MOV P4M0, #01H
MOV P4M1, #00H
; 设置P6.0为推挽输出
MOV P6M0, #0FFH
MOV P6M1, #00H

MOV SP, #80H
MOV P6, #0FFH
LCALL Hc595Off
CLR P4.0  ; 允许LED显示
MOV A, #0FEH
Loop:
MOV P6, A
RL A
LCALL DELAY500MS
SJMP Loop

; * @brief       向74HC595D发送一个字节
; * @param       ACC: 要发送的字节数据（存放在累加器中）
; * @retval      无
Hc595SendByte:
PUSH 02H
MOV R2, #8
Hc595SendLoop:
RLC A
MOV HC595_SI, C
SETB HC595_SCK
CLR HC595_SCK
DJNZ R2, Hc595SendLoop
POP 02H
RET

; * @brief       关闭74HC595D
; * @param       无
; * @retval      无
Hc595Off:
PUSH ACC
; 输出点阵位码，关闭LED点阵
CLR A
LCALL Hc595SendByte
; 输出数码管位码，关闭数码管
CLR A
LCALL Hc595SendByte
CLR HC595_RCK
NOP
SETB HC595_RCK  ; 输入数据上升沿锁存到输出端
CLR HC595_RCK  ; 锁存输出数据
POP ACC
RET

DELAY500MS:			;@24.000MHz
	NOP
	NOP
	PUSH	30H
	PUSH	31H
	PUSH	32H
	MOV		30H,#61
	MOV		31H,#225
	MOV		32H,#60
NEXT:
	DJNZ	32H,NEXT
	DJNZ	31H,NEXT
	DJNZ	30H,NEXT
	POP		32H
	POP		31H
	POP		30H
	RET

END