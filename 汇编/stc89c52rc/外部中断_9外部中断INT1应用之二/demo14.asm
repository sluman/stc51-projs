; ******************************************************************************
; * demo14.asm
; * LLP STUDIO SVP Peng
; * V1.0
; * 2023-08-26
; * 主控程序：数码管中的数字从0, 1, 2, …, 8, 9不断循环
; * INT1发生时，数字变化从9, 8, …, 2, 1, 0循环3圈，然后返回
; ******************************************************************************

ORG 0
JMP Start
ORG 13H
JMP Int_1

Start:
MOV IE, #10000100B
MOV SP, #30H
SETB IT1  ; 采用负缘触发信号

Renew:
MOV R0, #10  ; 输出次数
MOV R1, #1CH
MOV P2, R1
MOV R2, #0  ; ACC清零，设定初始输出值
MOV DPTR, #Tab  ; DPTR指向字形码表首地址

Loop:
MOV A, R2  ; 注意计数变量不能由A充当，因为A会被表指针覆盖
MOVC A, @A + DPTR
MOV P0, A
CALL Delay
INC R2
DJNZ R0, Loop
JMP Renew  ; 重新开始一轮数字显示
;以上部分为主控程序

; 中断INT1子程序开始
Int_1:
PUSH PSW
PUSH ACC
SETB RS0

; 循环开始
MOV R4, #2  ; 定义中断数码管内容9 - 0循环3圈，相对于源码而言为新增指令

Intre3:  ; 新增指令
MOV R0, #10  ; 设定10次循环
MOV R2, #9  ; 初始值，这里R2中的内容是BCD码，BCD码只有0, 1, ……, 8, 9

IntLoop1:
MOV A, R2
MOVC A, @A + DPTR
MOV P0, A
CALL Delay
DEC R2
DJNZ R0, IntLoop1
DJNZ R4, Intre3  ; 显示一轮完毕，新增程序

POP ACC
POP PSW
RETI  ; 返回主程序
; INT1中断子程序结束

Delay:  ; 0.5 s延时子程序（503018 μs）
MOV R7, #5
D1:
MOV R6, #200
D2:
MOV R5, #250
DJNZ R5, $
DJNZ R6, D2
DJNZ R7, D1
RET

Tab:
DB 3FH, 06H, 5BH, 4FH, 66H, 6DH, 7DH, 07H, 7FH, 6FH

END
