; ******************************************************************************
; * demo87_A.asm
; * LLP STUDIO SVP Peng
; * V1.0
; * 2023-08-26
; * 七段管的七段内容输出0，若有K4（P3.3）中断产生，七段输出内容0 - F循环加1
; ******************************************************************************

ORG 0000H

AJMP Main
ORG 0013H  ; 中断向量1程序入口
AJMP Int_1
ORG 0030H

Main:
SETB IT1     ; 中断触发方式：指定边沿触发方式（负缘触发）
SETB EX1     ; 中断使能方式：允许中断INT1使能
SETB EA      ; 中断使能方式：中断总开关使能
MOV R0, #0   ; 计数初值为0
MOV R1, #1CH
MOV P2, R1
MOV A, #3FH  ; "0"的字形码送至A

Al1:
MOV P0, A  ; 显示数码
MOV DPTR, #Tab  ; DPTR指向字形码表首地址
SJMP $  ; 无限循环，程序会停在这里等待中断

Int_1:
INC R0
MOV A, R0
ANL A, #0FH  ; 00001111和A逐位与，将A高四位屏蔽，防止R0不断加1超过F
MOVC A, @A + DPTR
MOV P0, A

Re:
RETI

Tab:
DB 3FH, 06H, 5BH, 4FH, 66H, 6DH, 7DH, 07H
DB 7FH, 6FH, 77H, 7CH, 39H, 5EH, 79H, 71H

END
