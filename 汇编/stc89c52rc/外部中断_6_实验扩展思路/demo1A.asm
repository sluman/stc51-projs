; ******************************************************************************
; * demo1A.asm
; * LLP STUDIO SVP Peng
; * V1.0
; * 2023-08-26
; * 七段管的七段输出8并闪烁若干次；若有中断产生，七段笔画A、B、C、D、E、F依次
; * 来回逐个点亮3轮，后又循环复原
; * 原始实验中断时单灯或笔画移动3圈后复原
; * 深入实验中断时单灯或笔画来回移动3圈后复原
; * 在普中开发板上由于P2.234引脚冲突，无法实现
; ******************************************************************************

ORG 0

JMP Start
ORG 03H
JMP Int_0

Start:
MOV IE, #10000001B
MOV SP, #30H
SETB IT0
MOV A, #0

Loop:
MOV P2, A
CALL Delay
CPL A
JMP Loop

; INT0中断子程序开始
Int_0:
PUSH ACC
PUSH PSW
SETB RS0
; 外层循环开始
MOV R0, #3

IntLoop0:
; 第2层循环1定义开始
MOV A, #0FEH
MOV R1, #8
IntLoop1:
MOV P2, A
CALL Delay
RL A
DJNZ R1, IntLoop1
; 第2层循环1结束

; 第2层循环2定义开始
MOV A, #07FH
MOV R1, #8
IntLoop2:
MOV P2, A
CALL Delay
RR A
DJNZ R1, IntLoop2
; 第2层循环2结束

DJNZ R0, IntLoop0
; 外层循环结束

POP PSW
POP ACC
RETI

Delay:
MOV R7, #200
D1:
MOV R6, #250
DJNZ R6, $
DJNZ R7, D1
RET

END
