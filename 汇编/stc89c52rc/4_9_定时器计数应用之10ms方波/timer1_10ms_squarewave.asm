; ******************************************************************************
; * timer1_10ms_squarewave.asm
; * LLP STUDIO SVP Peng
; * V1.0
; * 2023-08-31
; * 在P1.0上产生周期为10 ms的方波，设晶振频率12 MHz
; * 方波周期：10 ms -> 半个方波周期：5 ms
; * 5 ms ÷ 1 μs = 5000 次
; * 选择定时器Timer1工作方式1，定时器最大计数值：2^16 = 65536 = 0FFFFH + 1
; * 初值：65536 - 5000 = 60536 = 0EC78H
; ******************************************************************************

ORG 0000H
; 方法一：查询方式
;MOV TMOD, #10H  ; 将Timer1设定为16位定时器/计数器，TL1、TH1全用
;MOV TH1, #0ECH
;MOV TL1, #78
;SETB TR1  ; 允许T1开始计数

;Wait:
;; Jump if bit is set and clear it. 如果TF1置1（计数溢出），则跳转到NX并将TF1清零
;JBC TF1, NX
;SJMP Wait

;NX:
;CPL P1.0
;MOV TH1, #0ECH
;MOV TL1, #78H
;SJMP Wait

; 方法二：中断方式
ORG 0000H
MOV TMOD, #10H
MOV TH1, #0ECH
MOV TL1, #78H
MOV IE, #88H  ; 开启总中断和ET1
SETB TR1

Wait:
SJMP Wait  ; 等待中断

; 中断子程序
ORG 001BH
CPL P1.0
MOV TH1, #0ECH
MOV TL1, #78H
RETI

END
