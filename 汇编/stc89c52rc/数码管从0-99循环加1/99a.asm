; ******************************************************************************
; * 99a.asm
; * LLP STUDIO SVP Peng
; * V1.0
; * 2023-08-18
; * 数码管从0 - 99循环加1
; ******************************************************************************

ones EQU 20h  ; 个位数存放处
tens EQU 21h  ; 十位数存放处
count EQU 22h  ; 计数器寄存器

Start:
MOV count, #0  ; 初始化计数器
StartLoop:
ACALL Display
INC count
MOV A, count
CJNE A, #100, Next  ; 计数器到100重来
MOV count, #0
Next:
LJMP StartLoop

; 显示子程序
Display:
MOV A, count  ; 将count中的数转换成10进制
MOV B, #10
DIV AB  ; 商在A中，余数在B中
MOV tens, A  ; 十位在A
MOV ones, B  ; 个位在B
MOV DPTR, #Table
MOV R0, #2
Display1:
MOV R1, #100
DisplayLoop:
; 个位
MOV A, #18H
MOV P2, A
MOV A, ones  ; 取个位数
MOVC A, @A + DPTR
MOV P0, A
ACALL Delay
; 十位
MOV A, #1CH
MOV P2, A
MOV A, tens
MOVC A, @A + DPTR
MOV P0, A
ACALL Delay
DJNZ R1, DisplayLoop  ; 100次循环
DJNZ R0, Display1     ; 2个100次循环
RET

; 延时4 ms即 2 μs * R7 * R6（按12 MHz算）
Delay:
MOV R7, #2
D1:
MOV R6, #50
DJNZ R6, $
DJNZ R7, D1
RET

; 共阴的7段花数
Table:
DB 3FH, 06H, 5BH, 4FH, 66H, 6DH, 7DH, 07H
DB 7FH, 6FH, 77H, 7CH, 39H, 5EH, 79H, 71H

END
