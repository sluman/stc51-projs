; ******************************************************************************
; * demo_pulse.asm
; * LLP STUDIO SVP Peng
; * V1.0
; * 2023-09-01
; * 测量正脉冲宽度
; ******************************************************************************
; P3.2引脚接入外部信号发生器
; 当门控位GATE为1时，设定为外部启动，需要TRx = 1，INTx = 1才能启动定时器
; 利用上述特性可以测量外部输入的脉冲宽度
; 利用T0门控位测试INT0引脚上出现的正脉冲宽度，将测得的传递到4位数码管，
; LED8和7储存高位，LED6和5储存低位
; 分析：
; T0工作于定时方式1（16位计数），GATE设为1,
; 测试时，应在INT0为低电平时，设置TR0为1；当TR0变为高电平时，就启动计数；
; INT0再次变为低时，停止计数，此计数值与机器周期的乘积即为被测正脉冲的宽度

TL0_ONES EQU 20H
TL0_TENS EQU 21H
TH0_ONES EQU 22H
TH0_TENS EQU 23H

ORG 0000H
MOV TMOD, #09H  ; 定时计数器T0采用工作方式1，GATE = 1，所以为外部启动
MOV TL0, #00H  ; 设定计数初值为可最大计数
MOV TH0, #00H
JB P3.2, $  ; 若P3.2 = 1，则循环等待，等P3.2引脚变低
SETB TR0  ; P3.2 = 0，i.e. 低电平状态，则启动T0，准备工作
JNB P3.2, $  ; 若P3.2 = 0，则等待变高开始工作
JB P3.2, $  ; 计数开始，并等待P3.2变低，若不变则继续计数
CLR TR0  ; 若P3.2变低，停止计数
MOV A, TL0
MOV B, #10
DIV AB  ; 十位在A，个位在B
MOV TL0_TENS, A
MOV TL0_ONES, B
MOV A, TH0
MOV B, #10
DIV AB
MOV TH0_TENS, A
MOV TH0_ONES, B

ScanDisp:
MOV DPTR, #Table
Loop1:
MOV R0, #1CH  ; 0001 1100
MOV P2, R0    ; 138译码器译中/Y7，位选LED8
MOV A, TH0_TENS
MOVC A, @A + DPTR
MOV P0, A
CALL Delay
Loop2:
MOV R0, #18H  ; 0001 1000
MOV P2, R0    ; 138译码器译中/Y6，位选LED7
MOV A, TH0_ONES
MOVC A, @A + DPTR
MOV P0, A
CALL Delay
Loop3:
MOV R0, #14H  ; 0001 0100
MOV P2, R0    ; 138译码器译中/Y5，位选LED6
MOV A, TL0_TENS
MOVC A, @A + DPTR
MOV P0, A
CALL Delay
Loop4:
MOV R0, #10H  ; 0001 0000
MOV P2, R0    ; 138译码器译中/Y4，位选LED5
MOV A, TL0_ONES
MOVC A, @A + DPTR
MOV P0, A
CALL Delay
JMP ScanDisp

; 加快延时为4168 μs
Delay:
MOV R5, #5
D1:
MOV R6, #10
D2:
MOV R7, #40
DJNZ R7, $
DJNZ R6, D2
DJNZ R5, D1
RET

Table:
DB 0X3F, 0X06, 0X5B, 0X4F, 0X66, 0X6D, 0X7D, 0X07
DB 0X7F, 0X6F, 0X77, 0X7C, 0X39, 0X5E, 0X79, 0X71

END