; ******************************************************************************
; * digital_tube_dynamic_display2.asm
; * LLP STUDIO SVP Peng
; * V1.0
; * 2023-08-19
; * 数码管根据P3.1引脚状态分别输出12345或HELLO，K1松开为HELLO，按下为12345
; * 李群芳 肖看 《单片机原理、接口及应用》P78，孙安青 P60第13项实验，周润景百例，
; * 原始代码处理余辉不理想
; ******************************************************************************

ORG 000H
;MOV P2, #0  ; 数码管清显示

Test:
;SETB P3.1  ; 初始化开关电平
JB P3.1, Dir1  ; 检测开关，若P3.1 = 1，则跳转至Dir1执行
MOV DPTR, #Table1  ; 开关松开，指向"12345"字节形表头地址
SJMP Dir

Dir1:
MOV DPTR, #Table2  ; 开关按下，指向"HELLO"字节形表头地址

Dir:
MOV R0, #0   ; R0中存放字形表偏移量
MOV R1, #1CH  ; R1中存放数码表位选代码，即片选

Next:
MOV A, R0
MOVC A, @A + DPTR  ; 查字形码表
MOV P0, A  ; 送P0口输出
MOV A, R1
MOV P2, A  ; 片选，指定数码管显示相应内容
LCALL Delay
;MOV P2, #0  ; 数码管清显示，处理余辉乱码
;LCALL Delay
INC R0  ; 指向下一位字形
SUBB A, #4  ; 指向下一位数码管
MOV R1, A
CJNE R1, #08H, Next  ; #08H = 0000 1000B
SJMP Test

Delay:
MOV R6, #5
Delay2:
;MOV R7, #0F9H  ; 更正为250或应为#F9H
MOV R7, #50
Delay1:
NOP
NOP
DJNZ R7, Delay1
DJNZ R6, Delay2
RET

Table1:
DB 06H, 5BH, 4FH, 66H, 6DH  ; "1 - 5"字形码

Table2:
DB 76H, 79H, 38H, 38H, 3FH  ; "HELLO"字形码

END
