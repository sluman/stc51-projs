; ******************************************************************************
; * timer1_1s.asm
; * LLP STUDIO SVP Peng
; * V1.0
; * 2023-08-31
; * P2口8个LED同时亮1 s、灭1 s，周而复始
; ******************************************************************************
; 16位定时最大为2^16 = 65.536 ms，显然不能满足定时1 s的要求
; 方法一：
; 利用Timer0产生周期为100 ms的脉冲
; i.e. P3.5每50 ms取反一次作为Timer1的计数脉冲
; 利用Timer1对P3.5的下降沿计数，计10个脉冲正好100 ms * 10 = 1 s
; Timer0采用工作模式1：
; 应计脉冲数：50 ms / 1 μs = 50000 次
; 计数初值：15536 = 3CB0H
; Timer1采用工作模式2：
; 计数初值：2^8 - 10 = 246 = 0F6H

ORG 0000H

Main:
SETB P3.5
MOV P2, #0xFF
MOV TMOD, #61H  ; 将T1配置为计数器，8位自动重装载；T0配置为16位定时器
MOV TH1, #0F6H
MOV TL1, #0F6H  ; 计数10个脉冲
SETB TR1  ; 启动Timer1工作

Loop1:
CPL A
MOV P2, A
Loop2:
MOV TH0, #3CH
MOV TL0, #0B0H  ; 定时100 ms
SETB TR0  ; 启动Timer0工作
Loop3:
; 100 ms到，i.e. TCON寄存器中的TF0 = 1（自动），转至Loop4，并清TF0
JBC TF0, Loop4
SJMP Loop3  ; 未到100 ms，再次检查TF0
Loop4:
CPL P3.5
JBC TF1, Loop1
AJMP Loop2

END
