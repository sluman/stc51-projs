ORG 0000H  ;程序起始位置（伪指令）
;MOV A, #11111110B  ;存入欲显示灯的位置数据给累加器A
;MOV A, #0FEH

;MOV A, #0FH  ;当高四位为0-9，不需要加前导0
;MOV P2, A

;MOV P2, #0F0H

;CLR P2.7  ;清0寄存器 P2^7也可以
;JMP $  ;保持当前状态（程序在当前位置停止）

JMP Start
Start: MOV A, #00H  ;若不加#，00H意味着寄存器地址，而不是立即数
	   MOV P2, A
;	   JMP $
	   RET  ;返回
END  ;结束也是伪指令
