; 本程序为驱动一位共阴极数码管从0 - F显示

ORG 00H

; DPTR: 1个16位的数据指针寄存器，由两个8位寄存器DPH（Data Pointer High Byte）和
; DPL（Data Pointer Low Byte）组合而成
; DPTR一般为数据段的首地址
Main:
MOV R1, #1CH  ; 0001 1100
MOV P2, R1    ; 138译码器译中/Y7，位选LED8
MOV DPTR, #Table  ; 把后面表值存入DPTR
MOV R0, #0
;MOV R0, #15

Loop:
MOV A, R0
; @ 寄存器间接寻址前缀，@A类似于指向后面DPTR寄存器的指针
MOVC A, @A + DPTR  ; 从DPTR开始偏移A（R0）个地址的数读取到累加器A中
MOV P0, A
CALL Delay
INC R0  ; R0自增1
; CJNE 不相等则转移
CJNE R0, #16, Loop  ; R0为16的时候才跳出循环
;DJNZ R0, Loop
JMP Main

Delay:
MOV R5, #50
D1:
MOV R6, # 100
D2:
MOV R7, #100
DJNZ R7, $
DJNZ R6, D2
DJNZ R5, D1
RET

Table:
; DB: "Define Byte" 告诉汇编器在当前地址分配一个或多个字节的存储空间，
; 并初始化这些字节为指定的值
; 共阴极段码
DB 0X3F, 0X06, 0X5B, 0X4F, 0x66, 0X6D, 0X7D, 0X07
DB 0X7F, 0X6F, 0X77, 0X7C, 0X39, 0X5E, 0X79, 0X71
; 共阳极段码
;DB 0XC0, 0XF9, 0XA4, 0XB0, 0X99, 0X92, 0X82, 0XF8
;DB 0X80, 0X90, 0X88, 0X83, 0XC6, 0XA1, 0X86, 0X8E

END
