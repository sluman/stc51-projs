; ******************************************************************************
; * dec2hex.asm
; * LLP STUDIO SVP Peng
; * V1.0
; * 2023-10-21
; * 5位十进制数（BCD码）转成2B十六进制数
; ******************************************************************************

XM EQU 35H  ; 2B十六进制数高8位 m
XN EQU 36H  ; 2B十六进制数低8位 n

ORG 0000H
LJMP Main
ORG 0100H
Main:
MOV SP, #6FH
MOV R0, #30H
; * @brief       5位十进制数（BCD码）转2B十六进制数
; * @param       30H - 34H单元的5位数
; * @retval      XM（m） XN（n）
Dec2Hex:
PUSH PSW  ; 保护断点现场
PUSH ACC
PUSH B
PUSH 07H  ; R7入栈保护，使用R7暂存数据
MOV XM, #0  ; m = 0
MOV A, @R0  ; (A) = a
MOV XN, A  ; n = a
MOV R2, #4  ; 循环次数：4
ConvertLoop:
; mn * 10
; n * 10
MOV A, XN
MOV B, #10
MUL AB
MOV XN, A
MOV R7, B  ; n * 10的高8位进R7暂存
; m * 10
MOV B, #10
MOV A, XM
MUL AB  ; (A) = (m * 10)L
ADD A, R7  ; 丢弃(m * 10)H，因为积不超过2B
MOV XM, A  ; 完成mn * 10
MOV A, XN  ; (A) = (mn * 10)L
INC R0  ; R0指向下一位十进制数
ADD A, @R0  ; 加下一位十进制数
MOV XN, A  ; (XN) = (mn * 10)L + 下一位数
MOV A, XM  ; (A) = (mn * 10)H
ADDC A, #0  ; (mn * 10)H + Cy
MOV XM, A
DJNZ R2, ConvertLoop
POP 07H  ; 恢复断点现场
POP B
POP ACC
POP PSW
SJMP $
END
