/**
  * 单片机P2口接8个LED，每次点亮2只，先从右边向左边移动点亮，
  * 再从左边向右边移动点亮，然后闪烁2次，重复循环
  */

ORG 00H

Start: MOV R0, #8  ;在R0中存入立即数8：移动次数
;	   MOV A, #0FEH  ;存入第一个灯亮的位置
	   MOV A, #7FH

Loop1: MOV P2, A
	   CALL Delay
;	   RL A  ;左移一位 1111 1110 -> 1111 1100
	   RR A  ;右移一位
	   DJNZ R0, Loop1
	   ;在上一个循环的结尾将下一个循环要用的参数先存入寄存器，
	   ;否则下一个循环中该参数不变
	   MOV R1, #3  ;为下面全亮 -> 全灭 -> 全亮 -> 重新开始 做准备
	   MOV A, #00H
;	   JMP Start
Loop2: MOV P2, A  ;点亮所有的灯
	   CALL Delay
	   CPL A  ;对A按位取反，熄灭所有的灯
	   DJNZ R1, Loop2
;	   MOV A, #0FFH
;	   MOV P2, A
;	   CALL Delay
	   JMP Start

Delay: MOV R5, #50
D1: MOV R6, # 100
D2: MOV R7, #100
	DJNZ R7, $
	DJNZ R6, D2
	DJNZ R5, D1
	RET

END
