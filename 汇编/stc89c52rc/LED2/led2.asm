ORG 00H
Start:
	CLR P2.0
	CALL Delay
	SETB P2.0
	CALL Delay
	JMP Start

/* 50 ms 延时: ((2 * 100 + 3) * 100 + 3) * 25 + 3 = 507578 μs */
;Delay:
;	MOV R5, #25
;Delay1:
;	MOV R6, #100
;Delay2:
;	MOV R7, #100
;	DJNZ R7, $
;	DJNZ R6, Delay2
;	DJNZ R5, Delay1
;	RET

/* 1 s 延时: ((2 * 248 + 3) * 200 + 3) * 10 + 3 = 998033 μs */
Delay:
	MOV R5, #10
Delay1:
	MOV R6, #200
Delay2:
	MOV R7, #248
	DJNZ R7, $
	DJNZ R6, Delay2
	DJNZ R5, Delay1
	RET

END
