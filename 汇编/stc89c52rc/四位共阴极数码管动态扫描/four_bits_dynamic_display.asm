ORG 00H

Main:
MOV DPTR, #Table

Loop1:
MOV R0, #1CH  ; 0001 1100
MOV P2, R0    ; 138译码器译中/Y7，位选LED8
MOV A, #1
MOVC A, @A + DPTR
MOV P0, A
CALL Delay
Loop2:
MOV R0, #18H  ; 0001 1000
MOV P2, R0    ; 138译码器译中/Y7，位选LED7
MOV A, #2
MOVC A, @A + DPTR
MOV P0, A
CALL Delay
Loop3:
MOV R0, #14H  ; 0001 0100
MOV P2, R0    ; 138译码器译中/Y7，位选LED6
MOV A, #3
MOVC A, @A + DPTR
MOV P0, A
CALL Delay
Loop4:
MOV R0, #10H  ; 0001 0000
MOV P2, R0    ; 138译码器译中/Y7，位选LED5
MOV A, #4
MOVC A, @A + DPTR
MOV P0, A
CALL Delay
JMP Main

; 加快延时为4168 μs
Delay:
MOV R5, #5
D1:
MOV R6, #10
D2:
MOV R7, #40
DJNZ R7, $
DJNZ R6, D2
DJNZ R5, D1
RET

Table:
DB 0X3F, 0X06, 0X5B, 0X4F, 0X66, 0X6D, 0X7D, 0X07
DB 0X7F, 0X6F, 0X77, 0X7C, 0X39, 0X5E, 0X79, 0X71

END