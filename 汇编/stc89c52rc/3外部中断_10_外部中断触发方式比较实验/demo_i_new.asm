; ******************************************************************************
; * demo_i_new.asm
; * LLP STUDIO SVP Peng
; * V1.0
; * 2023-08-26
; * 外部中断INT0与INT1的触发方式比较研究实验
; * K3（INT0 P3.2）低电平触发LED1长亮 优先级高
; * K4（INT1 P3.3）下降沿触发LED2短亮
; ******************************************************************************

ORG 0000H
JMP Start
ORG 0003H
JMP Int_0
ORG 0013H
JMP Int_1

Start:
MOV IE, #10000101B
MOV SP, #30H
MOV P2, #0xFF
CLR IT0   ; IT0清零 低电平触发
SETB IT1  ; IT1置位 下降沿触发
SJMP $    ; 无限循环，等待中断

Int_0:
CLR P2.0
CALL Delay
SETB P2.0  ; 重新熄灭LED1
RETI

Int_1:
CLR P2.1
CALL Delay
SETB P2.1  ; 重新熄灭LED2
RETI

Delay:
MOV R5, #254
D1:
MOV R6, #14
D2:
MOV R7, #139
DJNZ R7, $
DJNZ R6, D2
DJNZ R5, D1
RET

END